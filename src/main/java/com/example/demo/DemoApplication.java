package com.example.demo;


import com.example.demo.utils.RandomGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;


@SpringBootApplication
public class DemoApplication {


	public static void main(String[] args) throws Exception {

        System.setProperty("java.net.preferIPv4Stack" , "true");
 		SpringApplication.run(DemoApplication.class, args);

	}


}
