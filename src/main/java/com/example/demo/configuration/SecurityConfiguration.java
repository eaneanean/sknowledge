package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(passwordEncoder);
        return provider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/browse")
                .and()
                .authorizeRequests()
                .antMatchers("/home").hasRole("USER")
                .antMatchers("/browse").hasRole("USER")
                .antMatchers("/browse/*").hasRole("USER")
                .antMatchers("/image").hasRole("USER")
                .antMatchers("/language").hasRole("USER")
                .antMatchers("/learner").hasRole("USER")
                .antMatchers("/teacher").hasRole("USER")
                .antMatchers("/write/*").hasRole("USER")
                .antMatchers("/preference").hasRole("USER")
                .antMatchers("/location").hasRole("USER")
                .antMatchers("/register").not().hasRole("USER")
                .antMatchers("/login").not().hasRole("USER")
                .antMatchers("/description").hasRole("USER")
                .antMatchers("/preference").hasRole("USER")
                .antMatchers("/home").hasRole("USER")
                .antMatchers("/star").hasRole("USER")
                .antMatchers("/").hasRole("USER")
                .antMatchers("/*").permitAll()
                .and()
                .rememberMe();
    }

    @Bean
    @Qualifier("BCrypt")
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
