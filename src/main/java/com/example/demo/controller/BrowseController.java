package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.Star;
import com.example.demo.dto.DetailedUserResult;
import com.example.demo.dto.UserResult;
import com.example.demo.service.DetailedUserResultService;
import com.example.demo.service.StarsService;
import com.example.demo.service.UserResultService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


@Controller
/**
 * Browse matched users and see the profile of
 * individual user
 */
public class BrowseController {

    @Autowired
    UserResultService userResultService;

    @Autowired
    UserService userService;

    @Autowired
    DetailedUserResultService detailedUserResultService;


    @Autowired
    StarsService starsService;


    @GetMapping({"/browse", "/", "/home"})
    public String handleGetRequest(@RequestParam(defaultValue = "1") int page,
                                   @RequestParam(defaultValue = "") String searchWord,
                                   @RequestParam(defaultValue = "dist_asc") String sort,
                                   Locale locale,
                                   Model model,
                                   @AuthenticationPrincipal User userPrincipal) throws IOException {

        long user = userPrincipal.getId();

        List<UserResult> list = userResultService.findAll(user, page, locale, searchWord, sort);

        PagedListHolder<UserResult> holder = new PagedListHolder<>(list);
        holder.setPageSize(6);
        holder.setPage(page - 1);

        for(int i = 0; i < holder.getPageSize(); i++) {
            try {
                String picture = userService.readBaseOfSmallPictureOrDefault(holder.getPageList().get(i).getId(),
                        holder.getPageList().get(i).getGender().getId());
                holder.getPageList().get(i).setSmallImage(picture);
            }catch (Exception e) {
                System.out.println(e);
            }

        }

        boolean isThereNext = !holder.isLastPage();
        boolean isTherePrevious = !holder.isFirstPage();

        String url = "/browse?sort="+ sort + "&" + "searchWord=" + searchWord + "&page=";

        int previosPage = page - 1;
        int nextPage = page + 1;

        String previousPageUrl = url + previosPage + "&user=" + user;
        String nextPageUrl = url + nextPage + "&user=" + user;

        System.out.println("Is there next:" + isThereNext);
        System.out.println("Is there previous:" + isTherePrevious);

        System.out.println("Previous page url:" + previousPageUrl);
        System.out.println("Next page url:" + nextPageUrl);



        model.addAttribute("userResultList", holder.getPageList());
        model.addAttribute("isTherePrevious", isTherePrevious);
        model.addAttribute("isThereNext", isThereNext);
        model.addAttribute("previous", previousPageUrl);
        model.addAttribute("next", nextPageUrl);
        model.addAttribute("searchWord", searchWord);

        return "browse";
    }


    @GetMapping("/browse/{id}")
    public String handle(@PathVariable("id") long id, Model model, Locale locale, @AuthenticationPrincipal User principalUser) {

        DetailedUserResult user = detailedUserResultService.findById(id, locale);

        String picture = null;

        try {
            picture = userService.readBaseOfBigPictureOrDefault(id, user.getGender().getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        user.setBigImage(picture);

        Star starsForm = new Star();

        starsForm.setGivenTo(id);

        System.out.println("Princial user:" + principalUser);


        if(starsService.findStar(principalUser.getId(), id) != null)
            starsForm.setStars(starsService.findStar(principalUser.getId(), id));


        model.addAttribute("user", user);
        model.addAttribute("picture", picture);
        model.addAttribute("star", starsForm);
        return "browseDetails";
    }

}
