package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.ChangePasswordForm;
import com.example.demo.service.UserService;
import com.example.demo.validator.ChangePasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
/**
 * Change the password both for logged in user and for users
 * who had previously clicked the link with the token for
 * changing email
 */
public class ChangePasswordController {

    @Autowired
    @Qualifier("BCrypt")
    PasswordEncoder encoder;

    @Autowired
    ChangePasswordValidator validator;

    @Autowired
    UserService userService;

    @GetMapping("/changePassword")
    public String handleGetRequest(@AuthenticationPrincipal User userPrincipal,
                                   HttpSession session, Model model) {

        User user;
        User userFromSession = (User) session.getAttribute("USER");

        if(userPrincipal != null) {
            user = userPrincipal;
            System.out.println("User principal is not null");
        }
        else if(userFromSession != null) {
            user = userFromSession;
            System.out.println("User from session is not null");
        }
        else {
            System.out.println("get method /changePassword");
            System.out.println(userPrincipal);
            System.out.println("User2:" + userFromSession);
            model.addAttribute(new ChangePasswordForm());
            return "changePasswordError";
        }

        model.addAttribute(new ChangePasswordForm());
        model.addAttribute(user);

        return "changePassword";
    }

    @PostMapping("/changePassword")
    public String handlePostRequest(@AuthenticationPrincipal User userPrincipal,
                                    @Valid ChangePasswordForm form, HttpSession session,
                                    BindingResult result) {

        validator.validate(form, result);

        if(result.hasErrors()) {
            return "changePassword";
        }

        User user = new User();
        User userFromSession = (User) session.getAttribute("USER");

        if(userPrincipal != null) {
            user = userPrincipal;
            System.out.println("User principal is not null from POST /changePasswor");
        }
        else if(userFromSession != null) {
            user = userFromSession;
            System.out.println("User from session is not null from POST /changePassword");
        }

        String encryptedPassword = encoder.encode(form.getPassword());

        user.setEncryptedPassword(encryptedPassword);
        userService.updateUser(user);

        return "redirect:/";
    }


}
