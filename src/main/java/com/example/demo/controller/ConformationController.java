package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.Token;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Controller
/**
 * After the user fills the register, he should get a conformation mail that must be
 * confirmed so that the user can be considered a register user.
 */
public class ConformationController {

    @Autowired
    TokenService tokenService;

    @Autowired
    UserService userService;


    @GetMapping("/conformation")
    public String handleConformation() {
        return "conformation";
    }

    @GetMapping("/conformation/{token}")
    public String handleConformationWithTokenString(
            @PathVariable("token") String tokenString, Locale locale, Model model,
            HttpServletRequest request) {

        try {
            Token token = tokenService.findTokenByTokenText(tokenString);

            if(token.isTokenExpired(1000*60*60))
                return "error";

            User user = userService.findUserById(token.getUserId(), locale);
            user.setEnabled(true);
            userService.updateUser(user);
            model.addAttribute(user);
            System.out.println("User from conformation controller:" + user);
            request.getSession().setAttribute("USER", user);
            return "redirect:/login";
        }
        catch (EmptyResultDataAccessException exception) {
            return "invalidToken";
        }


    }




}
