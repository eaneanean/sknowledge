package com.example.demo.controller;

import com.example.demo.dto.ContactForm;
import com.example.demo.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.swing.*;

@Controller
/**
 * Controller that handles situation when the user
 * wants to contact the admin of the web site
 */
public class ContactController {

    @Autowired
    ContactService contactService;

    @GetMapping("/contact")
    public String handleGetRequest(Model model) {

        model.addAttribute(new ContactForm());

        return "contact";
    }


    @PostMapping("/contact")
    public String handlePostRequest(ContactForm contactForm) {

        System.out.println(contactService.saveContactService(contactForm));

        return "redirect:/home";
    }


}
