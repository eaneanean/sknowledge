package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.DescriptionForm;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Locale;

@Controller
/**
 * Controller when the user gives description of himself/herself
 */
public class DescriptionController {

    @Autowired
    UserService userService;

    @GetMapping("/description")
    public String handleGetRequest(@AuthenticationPrincipal User user,
                                   Locale locale,
                                   Model model) {

        User databaseUser = userService.findUserById(user.getId(), locale);

        String existingDescription = "";

        if(databaseUser.getDescription() != null)
            existingDescription = databaseUser.getDescription();

        DescriptionForm descriptionForm = new DescriptionForm();
        descriptionForm.setDescriptionText(existingDescription);

        model.addAttribute(descriptionForm);

        return "description";
    }


    @PostMapping("/description")
    public String handlePostRequest(@AuthenticationPrincipal User user, DescriptionForm form) {

        user.setDescription(form.getDescriptionText());

        userService.updateUser(user);


        return "redirect:/browse";
    }

}
