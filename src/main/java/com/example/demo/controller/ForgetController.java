package com.example.demo.controller;


import com.example.demo.dto.User;
import com.example.demo.dto.ForgetForm;
import com.example.demo.dto.Token;
import com.example.demo.service.ConformationService;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;
import com.example.demo.validator.ForgetFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Locale;

@Controller
/**
 * Handles forget password form
 */
public class ForgetController {


    @Autowired
    ForgetFormValidator forgetFormValidator;

    @Autowired
    UserService userService;

    @Autowired
    TokenService tokenService;

    @Autowired
    ConformationService conformationService;


    @GetMapping("/forget")
    public String handleGetRequest(Model model, Locale locale) {

        model.addAttribute(new ForgetForm());

        return "forget";
    }

    @PostMapping("/forget")
    public String handlePostRequest(@Valid ForgetForm forgetForm, BindingResult bindingResult,
                                    Locale locale, HttpServletRequest request) throws MalformedURLException, URISyntaxException {


        forgetFormValidator.validateWithLocale(forgetForm, bindingResult, locale);


        if(bindingResult.hasErrors()) {
            System.out.println("Greska");
            return "forget";
        }

        User user = userService.findUserByMail(forgetForm.getEmail(), locale);
        Token token = tokenService.createToken(user);
        tokenService.saveToken(token);

        conformationService.sendChangePassword(forgetForm.getEmail(),
                token,
                conformationService.getURI(request, "/password"));

        return "redirect:/login";


    }



}
