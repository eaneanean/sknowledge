package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
/**
 * Handles action with the profile picture
 */
public class ImageController {

    @Autowired
    UserService userService;


    @GetMapping("/image")
    public String handleGetRequest(Model model, @AuthenticationPrincipal User user) {

        try {
            String picture = userService.readBaseOfBigPictureOrDefault(user.getId(), user.getGender().getId());
            model.addAttribute("picture", picture);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "image";
    }

    @PostMapping("image")
    public String handlePostRequest(@RequestParam("imageFile") MultipartFile file,
                                    @AuthenticationPrincipal User user) throws IOException {

        userService.saveProfilePicture(file, user.getId());

        return "redirect:/browse";
    }


}
