package com.example.demo.controller;

import com.example.demo.dto.Language;
import com.example.demo.dto.User;
import com.example.demo.dto.LanguageForm;
import com.example.demo.dto.UserLanguage;
import com.example.demo.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Locale;

@Controller
/**
 * Handles languages that the user speaks
 */
public class LanguageController {

    @Autowired
    LanguageService languageService;

    @GetMapping("/language")
    public String handleGet(Model model, Locale locale,
                            @AuthenticationPrincipal User user) {

        List<Language> allLanguages = languageService.findAll(locale);

        List<UserLanguage> userLanguages = languageService.findForUser(user.getId(), locale);

        for(int i = userLanguages.size(); i < 10; i++) {

            UserLanguage userLanguage = new UserLanguage();
            // so nothing shows in the select option
            userLanguage.setLanguage("");
            userLanguage.setCode("-1");
            userLanguages.add(userLanguage);
        }

        UserLanguage emptyLanguage = new UserLanguage();
        emptyLanguage.setCode("-1");
        emptyLanguage.setLanguage("");
        allLanguages.add(0, emptyLanguage);

        LanguageForm form = new LanguageForm();
        form.setLanguages(userLanguages);


        model.addAttribute("allLanguages", allLanguages);
        model.addAttribute("languageForm", form);
        return "language";
    }

    @PostMapping("/language")
    public String handlePost(LanguageForm form,
                             @AuthenticationPrincipal User user) {

        languageService.replaceLanguages(user.getId(), form.getLanguages());

        return "redirect:/browse";
    }


}
