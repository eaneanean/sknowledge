package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.LearnerForm;
import com.example.demo.dto.Interest;
import com.example.demo.dto.InterestLearner;
import com.example.demo.service.InterestLearnerService;
import com.example.demo.service.InterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Locale;

@Controller
/**
 * Handles what the user wants to learn
 */
public class LearnerController {

    @Autowired
    InterestService interestService;

    @Autowired
    InterestLearnerService interestLearnerService;




    @GetMapping("/learner")
    public String handleGet(Model model, Locale locale, @AuthenticationPrincipal User user) {

        LearnerForm form = new LearnerForm();

        List<InterestLearner> interestLearnerList = interestLearnerService
                .findInterestsByUserId(user.getId(), locale);

        // we should show 10 InterestLearner so we put empty InterestLeanrer
        // to get to number of(if it isn't already 10)


        for(int i = interestLearnerList.size(); i < 10; i++) {

            InterestLearner interestLearner = new InterestLearner();
            // so nothing shows in the select option
            interestLearner.setInterestName("");
            interestLearner.setInterestId(-1);
            interestLearnerList.add(i, interestLearner);
        }

        form.setInterests(interestLearnerList);

        List<Interest> interestList = interestService.findAllInterests(locale);

        Interest interest = new Interest();
        interest.setName("");
        interest.setId(-1);
        interestList.add(0, interest);


        model.addAttribute("learnerForm", form);
        model.addAttribute("allInterests", interestList);



        return "learnerForm";
    }

    @PostMapping("/learner")
    public String handlePost(LearnerForm form, @AuthenticationPrincipal User user) {

        if(form == null)
            System.out.println("form is null");

        if(form.getInterests() == null)
            System.out.println("interests are null");

        interestLearnerService.replaceInterestLearner(user.getId(), form.getInterests());


        return "redirect:/browse";
    }


}
