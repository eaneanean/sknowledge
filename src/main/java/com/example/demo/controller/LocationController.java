package com.example.demo.controller;

import com.example.demo.dto.Location;
import com.example.demo.dto.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
/**
 * Handles the current location of the user
 */
public class LocationController {

    @Autowired
    UserService userService;

    @GetMapping("/location")
    public String handelGet(Model model) {

        model.addAttribute("locationForm", new Location());

        return "location";
    }


    @PostMapping("/location")
    public String handlePost(Location location, @AuthenticationPrincipal User authUser) {

        authUser.setLocation(location);
        userService.updateUser(authUser);
        return "redirect:/browse";
    }

}
