package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.Token;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Controller
/**
 * Change password by confirming the token sent to email
 */
public class PasswordConformationController {

    @Autowired
    TokenService tokenService;

    @Autowired
    UserService userService;



    @GetMapping("/password/{token}")
    public String handleGetRequest(@PathVariable("token") String tokenString, Locale locale, Model model,
                                   HttpServletRequest request) {

        try {
            Token token = tokenService.findTokenByTokenText(tokenString);

            if(token.isTokenExpired(1000*60*60))
                return "invalidToken";

            User user = userService.findUserById(token.getUserId(), locale);
            model.addAttribute(user);
            System.out.println("User from conformation controller:" + user);
            request.getSession().setAttribute("USER", user);
            return "redirect:/changePassword";
        }
        catch (EmptyResultDataAccessException exception) {
            return "invalidToken";
        }

    }


}
