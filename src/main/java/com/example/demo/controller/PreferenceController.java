package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.Preference;
import com.example.demo.service.PreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
/**
 * Handles users preference for matched users(such as age, distance)
 */
public class PreferenceController {

    @Autowired
    PreferenceService preferenceService;

    @GetMapping("/preference")
    public String handleGet(@AuthenticationPrincipal User principalUser,
                            Model model) {

        Preference preference = preferenceService.findPreference(principalUser.getId());

        if(preference == null)
            preference = new Preference(13, 99, 200);


        model.addAttribute("preferenceForm", preference);

        return "preference";
    }

    @PostMapping("/preference")
    public String handlePost(@AuthenticationPrincipal User principalUser,
                             Preference preference) {

        preferenceService.insertPreference(principalUser.getId(), preference);

        return "redirect:/browse";
    }


}
