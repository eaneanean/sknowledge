package com.example.demo.controller;

import com.example.demo.dto.Gender;
import com.example.demo.dto.User;
import com.example.demo.dto.RegisterForm;
import com.example.demo.dto.Token;
import com.example.demo.service.*;
import com.example.demo.validator.RegisterFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.*;

@Controller
public class RegisterController {

    private final String REGISTER_PATH = "register";;

    @Autowired
    UserService userService;

    @Autowired
    RegisterFormValidator registerFormValidator;

    @Autowired
    EmailService emailService;

    @Autowired
    ConformationService conformationService;

    @Autowired
    GenderService genderService;

    @Autowired
    TokenService tokenService;

    @Autowired
    ErrorService errorService;

    @GetMapping("/" + REGISTER_PATH)
    public String handleGetRequest(Model model, Locale locale, HttpServletRequest request) {

        System.out.println("You are inside GET register");

        model.addAttribute("messages", errorService.getErrorMessages(locale));
        model.addAttribute("registerForm", new RegisterForm());


        List<Gender> genderList = new ArrayList<>();
        genderService
                .findAll(LocaleContextHolder.getLocale())
                .iterator()
                .forEachRemaining(gender -> genderList.add(gender));

        model.addAttribute(genderList);

        return "register";
    }

    @PostMapping("/" + REGISTER_PATH)
    public String handlePostRequest(@Valid RegisterForm registerForm, BindingResult result,
                                    MultipartFile profilePicture, Locale locale,
                                    Model model, HttpServletRequest request ) throws IOException, URISyntaxException {

        System.out.println("handlePostRequest() is called from POST class RegisterController");

        if(userService == null)
            System.out.println("Userservice is null from RegisterController");
        else
            System.out.println("Userservice is not null from RegisterController");


        registerFormValidator.validateWithLocale(registerForm, result, locale);

        if(result.hasErrors()) {
            System.out.println("result.hasErrors from Register Controller");
            result.getAllErrors().stream().forEach( err -> System.out.println(err));
            List<Gender> genderList = new ArrayList<>();
            genderService
                    .findAll(LocaleContextHolder.getLocale())
                    .iterator()
                    .forEachRemaining(gender -> genderList.add(gender));

            model.addAttribute(genderList);
            return REGISTER_PATH;
        }


        User user = userService.formToUser(new User(), registerForm);
        userService.saveUser(user);



        try {
            String url = (conformationService.getURI(request, "/conformation"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Token token = tokenService.createToken(user);
        tokenService.saveToken(token);

        conformationService.sendConformationMail(user, token, conformationService.getURI(request, "/conformation") );

        System.out.println(registerForm);
        System.out.println(result.getAllErrors());

        List<Gender> genderList = new ArrayList<>();
        genderService
                .findAll(LocaleContextHolder.getLocale())
                .iterator()
                .forEachRemaining(gender -> genderList.add(gender));

        model.addAttribute(genderList);

        if(result.hasErrors()) {
            System.out.println("There are some errors");
            return REGISTER_PATH;
        }
        return "checkEmail";
    }

}
