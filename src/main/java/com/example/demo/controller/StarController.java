package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.Star;
import com.example.demo.service.StarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
/**
 * Handles stars given to user
 */
public class StarController {

    @Autowired
    StarsService starsService;

    @PostMapping("/star")
    public String handlePost(Star star, @AuthenticationPrincipal User user) {


        star.setGivenBy(user.getId());
        star.setCreatedAt(System.currentTimeMillis());

        starsService.saveOrReplace(star);




        return "redirect:/browse";
    }

}
