package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.TeacherForm;
import com.example.demo.dto.Interest;
import com.example.demo.dto.InterestTeacher;
import com.example.demo.service.InterestService;
import com.example.demo.service.InterestTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Locale;

@Controller
/**
 * Handles things that user already knows
 */
public class TeacherController {

    @Autowired
    InterestTeacherService interestTeacherService;

    @Autowired
    InterestService interestService;


    @GetMapping("/teacher")
    public String handleGet(Model model, Locale locale,
                            @AuthenticationPrincipal User user) {

        TeacherForm form = new TeacherForm();

        List<InterestTeacher> interestTeacherList = interestTeacherService
                .findInterestsByUserId(user.getId(), locale);

        // we should show 10 InterestLearner so we put empty InterestLeanrer
        // to get to number of(if it isn't already 10)


        for(int i = interestTeacherList.size(); i < 10; i++) {

            InterestTeacher interestTeacher = new InterestTeacher();
            // so nothing shows in the select option
            interestTeacher.setName(" ");
            interestTeacher.setInterestId(-1);
            interestTeacherList.add(interestTeacher);
        }

        form.setInterests(interestTeacherList);

        List<Interest> interestList = interestService.findAllInterests(locale);

        Interest interest = new Interest();
        interest.setName("");
        interest.setId(-1);
        interestList.add(0, interest);

        model.addAttribute("teacherForm", form);
        model.addAttribute("allInterests", interestList);


        return "teacherForm";
    }


    @PostMapping("/teacher")
    public String handlePost(TeacherForm form,
                             @AuthenticationPrincipal User user) {

        interestTeacherService.replaceInterestTeacher(user.getId(), form.getInterests());

        return "redirect:/browse";

    }

}
