package com.example.demo.controller;

import com.example.demo.dto.User;
import com.example.demo.dto.WriteForm;
import com.example.demo.service.ConformationService;
import com.example.demo.service.EmailService;
import com.example.demo.service.UserService;
import com.example.demo.utils.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Locale;

@Controller
/**
 * Write to the chosen user. The user will get an email with the message
 */
public class WriteController {

    @Autowired
    UserService userService;

    @Autowired
    ConformationService conformationService;

    @Autowired
    EmailService emailService;

    @Autowired
    MessageSource messageSource;


    @GetMapping("/write/{id}")
    public String handleGet(Model model, @PathVariable long id) {


        WriteForm writeForm = new WriteForm();
        writeForm.setUserId(id);

        model.addAttribute("writeForm", writeForm);
        return "write";
    }

    @PostMapping("/write")
    public String handlePost(WriteForm writeForm, Locale locale, HttpServletRequest request,
                             @AuthenticationPrincipal User principal) throws MalformedURLException, URISyntaxException {

        User user = userService.findUserById(writeForm.getUserId(), locale);
        Messages messages = new Messages(messageSource, locale);

        System.out.println(writeForm.getMessage());
        System.out.println(writeForm.getUserId());

        String urlToVisit = conformationService.getURI(request, "/browse/" + principal.getId());
        String messageToVisit = messages.getValue("email.write");

        emailService.sendSimpleMessage(user.getEmail(),
                messages.getValue("email.write.subject") + principal.getFirstName(),
                writeForm.getMessage() + "\n" + messageToVisit + urlToVisit);

        return "redirect:/browse";
    }



}
