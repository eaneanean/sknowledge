package com.example.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class ContactForm {

    @NonNull
    private String email;

    @NonNull
    private String message;

}
