package com.example.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class DescriptionForm {

    @NonNull
    private String descriptionText;

}
