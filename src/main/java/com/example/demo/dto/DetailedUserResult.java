package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class DetailedUserResult extends UserResult {

    private String description;

    private List<InterestLearner> interestLearner = new ArrayList<>();

    public void addInterestTeacher(InterestLearner interest) {
        this.interestLearner.add(interest);
    }

    @Override
    public String toString () {
        String superString = super.toString();
        StringBuilder sb = new StringBuilder();

        interestLearner.stream().forEach(sb::append);

        return superString + sb.toString();
    }


}
