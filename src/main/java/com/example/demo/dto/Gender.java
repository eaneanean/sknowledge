package com.example.demo.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class Gender {

    @NonNull
    private Integer id;
    @NonNull
    private String genderName;

    @Override
    public String toString() {
        return genderName;
    }

}
