package com.example.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The things that the user wants to learn
 */
@Data
@NoArgsConstructor
public class InterestLearner {

    String interestName;

    int interestId;
    // minimal level of knowledge that the teacher should have range (1-beginner, 5-expert)
    int minimalLevel;

    // the teacher should know at least one language that the learner
    // speaks with the given level (1-beginner, 5-expert)
    int minimalLevelOfLanguage;


}
