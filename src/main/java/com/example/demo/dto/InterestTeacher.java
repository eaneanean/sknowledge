package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The things that the user wants to learn
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InterestTeacher {

    String name;
    int interestId;
    int levelOfKnowledge;

}
