package com.example.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
public class RegisterForm {

    @NonNull
    @NotNull
    @Size(min = 2, max = 50, message = "{error.firstName}")
    private String firstName;

    @NonNull
    @NotNull
    @Size(min = 2, max = 50, message = "{register.lastName.error}")
    private String lastName;

    @NonNull
    @NotNull(message = "{register.age.error2}")
    @Min(13)
    @Max(99)
    private Integer age;

    @NonNull
    private Integer gender;

    @NonNull
    @NotNull(message = "{register.email.error}")
    @Email
    private String email;

    @NonNull
    @NotNull
    private double latitude;

    @NonNull
    @NotNull
    private double longitude;

    @NonNull
    @NotNull
    private String unencryptedPassword;

    @NonNull
    @NotNull
    private String unencryptedRepeatedPassword;



}
