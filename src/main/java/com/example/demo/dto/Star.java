package com.example.demo.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Star {

    int stars;
    long givenBy;
    long givenTo;
    long createdAt;

}
