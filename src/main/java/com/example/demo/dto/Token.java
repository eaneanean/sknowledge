package com.example.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.Instant;

@Data
@NoArgsConstructor
public class Token {

    @NonNull
    private Long tokenId;

    @NonNull
    private Long userId;

    @NonNull
    private long createdAt;

    @NonNull
    private String randomString;

    /**
     *
     * @param expirationTime in milliseconds. Must be bigger than or equal to 0.
     * @return whether the token is expired at the moment of the function call.
     */
    public boolean isTokenExpired(long expirationTime) {
        if(expirationTime < 0)
            throw new IllegalArgumentException(("Exception time must be bigger than 0"));
        return (createdAt + expirationTime) < Instant.now().toEpochMilli();
    }


}
