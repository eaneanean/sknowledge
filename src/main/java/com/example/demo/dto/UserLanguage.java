package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class UserLanguage extends Language {

    int proficiency;

    @Override
    public String toString() {
        return super.toString() + " proficiency: " + proficiency;
    }



}
