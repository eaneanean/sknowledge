package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class UserResult {

    private long id;

    private String firstName;

    private String lastName;

    private Gender gender;

    private int age;

    // Double instaed of double so it can be null
    private Double distance;

    private int numberOfPeopleWhoRatedTheUser;

    private double averageStars;

    private List<InterestTeacher> interestTeacher = new ArrayList<>();

    public void addInterestTeacher(InterestTeacher interest) {
        this.interestTeacher.add(interest);
    }

    private String smallImage;

    private String bigImage;

    private List<UserLanguage> languages = new ArrayList<>();

    public void addLanguage(UserLanguage language) {
        languages.add(language);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        interestTeacher.stream().forEach(sb::append);
        return "UserResult{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                ", distance=" + distance +
                ", numberOfPeopleWhoRatedTheUser=" + numberOfPeopleWhoRatedTheUser +
                ", averageStars=" + averageStars +
                ", interestTeacher=" + sb.toString() +
                '}' + " set size:" + interestTeacher.size();
    }
}
