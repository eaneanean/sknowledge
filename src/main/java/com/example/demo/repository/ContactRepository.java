package com.example.demo.repository;

import com.example.demo.dto.ContactForm;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

@Repository
public interface ContactRepository {

    long saveContact(ContactForm contactForm);
}
