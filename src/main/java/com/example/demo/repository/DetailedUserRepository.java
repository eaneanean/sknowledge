package com.example.demo.repository;

import com.example.demo.dto.DetailedUserResult;
import org.springframework.stereotype.Repository;

import java.util.Locale;

@Repository
public interface DetailedUserRepository {

    public DetailedUserResult findDetailedUserResultById(long id, Locale locale);

}
