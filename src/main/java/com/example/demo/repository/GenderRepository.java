package com.example.demo.repository;

import com.example.demo.dto.Gender;

import java.util.Locale;

public interface GenderRepository {

    public Iterable<Gender> findAll(Locale locale);
    public Gender findById(Locale locale, int id);
}
