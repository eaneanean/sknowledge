package com.example.demo.repository;

import com.example.demo.dto.User;

public interface ImageRepository {


    /**
     *
     * @param userId
     * @param imageName together with extension(example 1.png)
     * @return
     */
    public int saveImageName(long userId, String imageName);

    public String findImageByUserId(long userId);
    public String findImageByUser(User user);

}
