package com.example.demo.repository;

import com.example.demo.dto.InterestLearner;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;

@Repository
public interface InterestLearnerRepository {

    List<InterestLearner> findInterestsByUserId(long userId, Locale locale);
    int insertOrUpdateInterestsLearner(long userId, InterestLearner interestLearner);
    int deleteInterestsLearner(long userId, int interestId);
    int deleteAllInterestForUser(long userId);

    /**
     *
     * @param userId
     * @param list
     * @return an array of the number of rows affected by each statement
     * (may also contain special JDBC-defined negative
     * values for affected rows such as Statement.SUCCESS_NO_INFO/Statement.EXECUTE_FAILED)
     */
    int [] batchInsertInterestLearner(long userId, List<InterestLearner> list);

}
