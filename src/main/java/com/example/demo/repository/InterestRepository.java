package com.example.demo.repository;

import com.example.demo.dto.Interest;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;

@Repository
public interface InterestRepository {

    public List<Interest> findAllInterests(Locale locale);

}
