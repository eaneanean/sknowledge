package com.example.demo.repository;

import com.example.demo.dto.ContactForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.time.Instant;

import static java.time.Instant.*;

@Repository
public class JdbcContactRepository implements ContactRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcContactRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long saveContact(ContactForm contactForm) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final String sqlExpression = "INSERT INTO contact " +
                "(email, message, sendAt) " +
                "VALUES(?, ?, ?)";

        jdbcTemplate.update( connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(sqlExpression,  Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, contactForm.getEmail());
            ps.setString(2, contactForm.getMessage());
            ps.setLong(3, now().toEpochMilli());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }
}
