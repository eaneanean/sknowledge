package com.example.demo.repository;

import com.example.demo.dto.*;
import com.example.demo.utils.LocaleChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class JdbcDetailerUserResultRepository implements DetailedUserRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcDetailerUserResultRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public DetailedUserResult findDetailedUserResultById(long userId, Locale locale) {

        locale = LocaleChanger.changeLocale(locale);


        String isoCode = "\"" + locale.getLanguage() + "\"";



         // We are using sql statement inside of the program, because views(which
         // would be suitable for this kind of thins does not support named parameters
        // in MySQL)

        String sql = "\tSELECT * \n" +
                "\tFROM (\n" +
                "\tSELECT users.id as id, age, description, first_name, last_name, gender, gender_translated, interests_teacher.interest_id AS teacherInterestId, \n" +
                "\tinterests_teacher.level AS teacherLevel, interests_translation.name, interests_learner.interest_id, \n" +
                "\tinterests_learner.min_language_level, interests_learner.min_level, i2.name as learnerName, \n" +
                "\tlanguages_translation.language_translated as languageName, languages_translation.iso_code as isoCode, \n" +
                "\tproficiency\n" +
                "\tFROM users, gender_translation, interests_teacher, interests_learner, interests_translation, interests_translation i2,\n" +
                "\tlanguages_translation, users_languages\n" +
                "\tWHERE users.id = " + userId + "\n" +
                "\tAND gender = gender_translation.genderId\n" +
                "\tAND gender_translation.language_code = " + isoCode + "\n" +
                "\tAND users.id = interests_teacher.user_id\n" +
                "\tAND interests_teacher.interest_id = interests_translation.interest_id \n" +
                "\tAND interests_translation.iso_code = " + isoCode + "\n" +
                "\tAND users.id = interests_learner.user_id\n" +
                "\tAND i2.interest_id  = interests_learner.interest_id \n" +
                "\tAND i2.iso_code = " + isoCode + "\n" +
                "\tAND users_languages.user_id = users.id \n" +
                "\tAND users_languages.iso_code = languages_translation.iso_code \n" +
                "\tAND languages_translation.language_code = " + isoCode + "\n" +
                "\t) AS t1 LEFT JOIN star_user\n" +
                "\tON t1.id = star_user .given_to\n";

         ResultSetExtractor<DetailedUserResult>  rse = (ResultSet rs) -> {

            DetailedUserResult userResult = null;
            Map<Integer, InterestLearner> interestsLearnerMap = new HashMap<>();
            Map<Integer, InterestTeacher> interestsTeacherMap = new HashMap<>();
            Map<String, UserLanguage> languageMap = new HashMap<>();

            while (rs.next()) {

                if (userResult == null) {

                    userResult = new DetailedUserResult();
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    long id = rs.getLong("id");

                    int genderId = rs.getInt("gender");
                    String genderTranslated = rs.getString("gender_translated");
                    Gender gender = new Gender(genderId, genderTranslated);

                    int age = rs.getInt("age");

                    double average = rs.getDouble("average");
                    int total = rs.getInt("total");

                    String description = rs.getString("description");

                    userResult.setAge(age);
                    userResult.setFirstName(firstName);
                    userResult.setLastName(lastName);
                    userResult.setGender(gender);
                    userResult.setAverageStars(average);
                    userResult.setNumberOfPeopleWhoRatedTheUser(total);
                    userResult.setId(id);
                    userResult.setDescription(description);

                }

                int teacherInterestId = rs.getInt("teacherInterestId");
                int teacherLevel = rs.getInt("teacherLevel");
                String interestName = rs.getString("name");

                InterestTeacher interestTeacher = new InterestTeacher(interestName,
                        teacherInterestId,
                        teacherLevel);

                if (!interestsTeacherMap.containsKey(interestTeacher.getInterestId()))
                    interestsTeacherMap.put(interestTeacher.getInterestId(), interestTeacher);

                int learnerInterestId = rs.getInt("interest_id");
                int learnerLangaugeLevel = rs.getInt("min_language_level");
                String learnerName = rs.getString("learnerName");
                int minLevel = rs.getInt("min_level");

                InterestLearner interestLearner = new InterestLearner();
                interestLearner.setInterestName(learnerName);
                interestLearner.setInterestId(learnerInterestId);
                interestLearner.setMinimalLevel(minLevel);
                interestLearner.setMinimalLevelOfLanguage(learnerLangaugeLevel);

                if (!interestsLearnerMap.containsKey(interestLearner.getInterestId())) {
                    interestsLearnerMap.put(interestLearner.getInterestId(), interestLearner);
                }

                int proficiency = rs.getInt("proficiency");
                String languageIsoCode = rs.getString("isoCode");
                String languageName = rs.getString("languageName");

                UserLanguage language = new UserLanguage();
                language.setCode(languageIsoCode);
                language.setLanguage(languageName);
                language.setProficiency(proficiency);

                if(!languageMap.containsKey(language.getCode()))
                    languageMap.put(language.getCode(), language);


                } //while loop


                if(userResult == null)
                    System.out.println("userResult is null");

                userResult.setInterestTeacher(interestsTeacherMap
                        .values()
                        .stream()
                        .collect(Collectors.toList()));

                userResult.setInterestLearner(interestsLearnerMap
                .values()
                .stream()
                .collect(Collectors.toList()));

                userResult.setLanguages(languageMap
                .values()
                .stream()
                .collect(Collectors.toList()));

                return userResult;

            };



        return jdbcTemplate.query(sql, rse);

    };
}
