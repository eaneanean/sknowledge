package com.example.demo.repository;

import com.example.demo.dto.Gender;
import com.example.demo.utils.LocaleChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

@Repository
public class JdbcGenderRepository implements GenderRepository {


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcGenderRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Iterable<Gender> findAll(Locale locale) {

        locale = LocaleChanger.changeLocale(locale);

        return jdbcTemplate.query("SELECT genderId, gender_translated\n" +
                "FROM gender_translation\n" +
                "WHERE language_code = ?", this::mapGender,
                locale.getLanguage());
    }

    @Override
    public Gender findById(Locale locale, int id) {
        if(locale == null)
            System.out.println("locale is null");
        else
            System.out.println("locale is not null");

        locale = LocaleChanger.changeLocale(locale);

        if(jdbcTemplate == null)
            System.out.println("jdbc is null");
        else
            System.out.println("jdbc is not null");

        System.out.println("JdbcGenderRepository findById locale.getLanguage():" + locale.getLanguage());
        System.out.println("JdbcGenderRepository findById id:" + id);
        return jdbcTemplate.queryForObject("SELECT genderId, gender_translated " +
                "FROM gender_translation " +
                "WHERE language_code = ? AND " +
                "genderId = ?", this::mapGender,
                locale.getLanguage(), id);
    }

    private Gender mapGender(ResultSet resultSet, int rowNumber) throws SQLException {
        return new Gender(
                resultSet.getInt("genderId"),
                resultSet.getString("gender_translated"));
    }
}
