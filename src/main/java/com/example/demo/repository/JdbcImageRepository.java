package com.example.demo.repository;

import com.example.demo.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcImageRepository implements ImageRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public int saveImageName(long userId, String imageName) {
        return jdbcTemplate.update("INSERT INTO users_picture " +
                " (user_id, picture_name) VALUES (?, ?)",
                userId,
                imageName);
    }

    @Override
    public String findImageByUserId(long userId) {
        return jdbcTemplate.queryForObject("SELECT picture_name " +
                "FROM users_picture " +
                "WHERE user_id = ?", ((rs, i) -> rs.getString("picture_name")), userId);
    }

    @Override
    public String findImageByUser(User user) {
        return findImageByUserId(user.getId());
    }

}
