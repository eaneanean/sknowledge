package com.example.demo.repository;

import com.example.demo.dto.InterestLearner;
import com.example.demo.utils.LocaleChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

@Repository
public class JdbcInterestLearnerRepository implements InterestLearnerRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public List<InterestLearner> findInterestsByUserId(long userId, Locale locale) {

        locale = LocaleChanger.changeLocale(locale);

        return jdbcTemplate.query("SELECT interests_translation.interest_id, min_level, min_language_level, name " +
                "FROM interests_learner, interests_translation " +
                "WHERE user_id = ? AND " +
                "interests_learner.interest_id = interests_translation.interest_id AND " +
                        "iso_code = ?",
                new Object[]{userId, locale.getLanguage()}, this::mapFromUserId);
    }

    @Override
    public int deleteInterestsLearner(long userId, int interestId) {
        return jdbcTemplate.update("DELETE FROM interests_learner " +
                "WHERE user_id = ? AND interest_id = ?", userId, interestId);
    }

    @Override
    public int deleteAllInterestForUser(long userId) {
        return jdbcTemplate.update("DELETE FROM interests_learner " +
                "WHERE user_id = ?", userId);
    }

    @Override
    public int[] batchInsertInterestLearner(long userId, List<InterestLearner> list) {
        return this.jdbcTemplate.batchUpdate(
                "INSERT INTO interests_learner (user_id, interest_id, min_level," +
                        "min_language_level) VALUES (?, ?, ?, ?)",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                        preparedStatement.setLong(1, userId);
                        preparedStatement.setInt(2, list.get(i).getInterestId());
                        preparedStatement.setInt(3, list.get(i).getMinimalLevel());
                        preparedStatement.setInt(4, list.get(i).getMinimalLevelOfLanguage());
                    }

                    @Override
                    public int getBatchSize() {
                        return list.size();
                    }
                }
        );
    }


    @Override
    public int insertOrUpdateInterestsLearner(long userId, InterestLearner interestLearner) {
        return jdbcTemplate.update("INSERT INTO interests_learner " +
                "(user_id, interest_id, min_level, min_language_level) " +
                "VALUES (?, ?, ?, ?) " +
                " ON DUPLICATE KEY UPDATE min_level = VALUES(min_level), " +
                "min_language_level = VALUES(min_language_level)",
                userId,
                interestLearner.getInterestId(),
                interestLearner.getMinimalLevel(),
                interestLearner.getMinimalLevelOfLanguage());
    }

    private InterestLearner mapFromUserId(ResultSet resultSet, int i) throws SQLException {

        InterestLearner interestLearner = new InterestLearner();

        interestLearner.setInterestId(resultSet.getInt("interest_id"));
        interestLearner.setMinimalLevel(resultSet.getInt("min_level"));
        interestLearner.setMinimalLevelOfLanguage(resultSet.getInt("min_language_level"));
        interestLearner.setInterestName(resultSet.getString("name"));

        return  interestLearner;
    }
}
