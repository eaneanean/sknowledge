package com.example.demo.repository;

import com.example.demo.dto.Interest;
import com.example.demo.utils.LocaleChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.List;
import java.util.Locale;

@Repository
public class JdbcInterestRepository implements InterestRepository {

    JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcInterestRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Interest> findAllInterests(Locale locale) {

        locale = LocaleChanger.changeLocale(locale);

        String sql = "SELECT * \n" +
                "FROM interests, interests_translation \n" +
                "WHERE interests.id = interests_translation.interest_id \n" +
                "AND interests_translation.iso_code = ? \n" +
                "ORDER BY name ASC\n";


        return jdbcTemplate.query(sql, new Object[] {locale.getLanguage()}, (ResultSet rs, int i) -> {
            Interest interest = new Interest();

            interest.setId(rs.getInt("interest_id"));
            interest.setName(rs.getString("name"));

            return interest;
        });
    }
}
