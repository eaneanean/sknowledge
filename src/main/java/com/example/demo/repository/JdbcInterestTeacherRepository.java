package com.example.demo.repository;

import com.example.demo.dto.InterestTeacher;
import com.example.demo.utils.LocaleChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

@Repository
public class JdbcInterestTeacherRepository implements InterestTeacherRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<InterestTeacher> findInterestsByUserId(long userId, Locale locale) {

        locale = LocaleChanger.changeLocale(locale);

        return jdbcTemplate.query("SELECT interests_translation.interest_id as interest, level, name " +
                        "FROM interests_teacher, interests_translation " +
                        "WHERE user_id = ? AND " +
                        "interests_teacher.interest_id = interests_translation.interest_id AND " +
                        "iso_code = ? " +
                        "ORDER BY name ASC",
                new Object[]{userId, locale.getLanguage()}, this::mapFromUserId);
    }

    private InterestTeacher mapFromUserId(ResultSet resultSet, int i) throws SQLException {

        InterestTeacher interestTeacher = new InterestTeacher();

        interestTeacher.setInterestId(resultSet.getInt("interest"));
        interestTeacher.setName(resultSet.getString("name"));
        interestTeacher.setLevelOfKnowledge(resultSet.getInt("level"));

        return interestTeacher;
    }



    @Override
    public int deleteAllInterestForUser(long userId) {
        return jdbcTemplate.update("DELETE FROM interests_teacher " +
                "WHERE user_id = ?", userId);
    }

    @Override
    public int[] batchInsertInterestTeacher(long userId, List<InterestTeacher> list) {
        return this.jdbcTemplate.batchUpdate(
                "INSERT INTO interests_teacher (user_id, interest_id, level)" +
                " VALUES (?, ?, ?)",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                        preparedStatement.setLong(1, userId);
                        preparedStatement.setInt(2, list.get(i).getInterestId());
                        preparedStatement.setInt(3, list.get(i).getLevelOfKnowledge());
                    }

                    @Override
                    public int getBatchSize() {
                        return list.size();
                    }
                }
        );
    }
}
