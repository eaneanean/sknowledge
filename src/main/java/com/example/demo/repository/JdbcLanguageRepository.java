package com.example.demo.repository;

import com.example.demo.dto.Language;
import com.example.demo.dto.UserLanguage;
import com.example.demo.utils.LocaleChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

@Repository
public class JdbcLanguageRepository implements LanguageRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcLanguageRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public Language findOne(String isoCode) {

        if(jdbcTemplate == null) {
            throw new NullPointerException("jdbc seemes to be null");
        }

        return jdbcTemplate.queryForObject(
                "SELECT iso_code, language_translated " +
                        "FROM languages_translation " +
                        "WHERE iso_code = ?",
                this::mapIsoCodeToLanguage,
                isoCode
        );

    }

    @Override
    public List<Language> findAll(Locale locale) {

        locale = LocaleChanger.changeLocale(locale);

        return jdbcTemplate.query("SELECT iso_code, language_translated " +
                "FROM languages_translation " +
                "WHERE language_code = \"" + locale.getLanguage() + "\"" +
                " ORDER BY language_translated ASC", this::mapIsoCodeToLanguage);
    }

    @Override
    public List<UserLanguage> findForUser(long userId, Locale locale) {

        locale = LocaleChanger.changeLocale(locale);

        String sql = "SELECT\n" +
                "\tusers_languages.iso_code,\n" +
                "\tproficiency,\n" +
                "\tlanguage_translated\n" +
                "FROM\n" +
                "\tusers_languages,\n" +
                "\tlanguages_translation\n" +
                "WHERE\n" +
                "\tusers_languages.iso_code = languages_translation.iso_code\n" +
                "\tAND languages_translation.language_code = ? \n" +
                "\tAND user_id = ?;\n";

        return jdbcTemplate.query(sql, new Object[]{locale.getLanguage(), userId},
                (ResultSet rs, int i) -> {
                    UserLanguage userLanguage = new UserLanguage();

                    userLanguage.setLanguage(rs.getString("language_translated"));
                    userLanguage.setProficiency(rs.getInt("proficiency"));
                    userLanguage.setCode(rs.getString("users_languages.iso_code"));

                    return userLanguage;
                });
    }

    @Override
    public int deleteAllInterestForUser(long userId) {
        return jdbcTemplate.update("DELETE FROM users_languages " +
                "WHERE user_id = ?", userId);
    }

    @Override
    public int[] batchInsertLanguagesForUser(long userId, List<UserLanguage> list) {
        return this.jdbcTemplate.batchUpdate(
                "INSERT INTO users_languages (user_id, iso_code, proficiency)" +
                        " VALUES (?, ?, ?)",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                        preparedStatement.setLong(1, userId);
                        preparedStatement.setString(2, list.get(i).getCode());
                        preparedStatement.setInt(3, list.get(i).getProficiency());
                    }

                    @Override
                    public int getBatchSize() {
                        return list.size();
                    }
                }
        );
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private Language mapIsoCodeToLanguage(ResultSet rs, int rowNum) throws SQLException {
        return new Language(
                rs.getString("iso_code"),
                rs.getString("language_translated"));
    }


}
