package com.example.demo.repository;

import com.example.demo.dto.Preference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.List;

@Repository
public class JdbcPreferenceRepository implements PreferenceRepository{

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcPreferenceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public Preference findPreference(long id) {

        String sql = "SELECT *\n" +
                "FROM preferences \n" +
                "WHERE user_id = ?;\n";

        List<Preference> result = jdbcTemplate.query(sql, new Object[]{id}, (ResultSet rs, int rowN) -> {
             Preference preference = new Preference();
             preference.setMinAge(rs.getInt("min_age"));
             preference.setMaxAge(rs.getInt("max_age"));
             preference.setMaxDistance(rs.getInt("max_distance"));
             return preference;
        });

        if(result.size() == 0)
            return null;
        else
            return result.get(0);


    }

    @Override
    public int insertPreference(long id, Preference preference) {

        String sql = "REPLACE INTO preferences (user_id, min_age, max_age, max_distance) " +
                "VALUES (?, ?, ?, ?) ";

        return jdbcTemplate.update(sql, new Object[]{id, preference.getMinAge(),
                        preference.getMaxAge(), preference.getMaxDistance()});
    }
}
