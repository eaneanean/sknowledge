package com.example.demo.repository;

import com.example.demo.dto.Star;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.util.List;

@Repository
public class JdbcStarsRepository implements StarsRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Integer findStar(long fromId, long toId) {

        List<Integer> list = jdbcTemplate.query("SELECT number_of_stars" +
                " FROM stars " +
                " WHERE given_by = ? AND given_to = ?",
                new Object[]{fromId, toId}, (ResultSet rs, int rowNum ) -> {

                    return rs.getInt(1);
                });

        if(list.size() == 0)
            return null;
        else
            return list.get(0);

    }

    @Override
    public int saveOrReplace(Star star) {
        return jdbcTemplate.update("REPLACE INTO stars " +
                " (given_by, given_to, number_of_stars, created_at) " +
                " VALUES (?, ?, ?, ?);", new Object[]{star.getGivenBy(),
        star.getGivenTo(), star.getStars(), star.getCreatedAt()});
    }

    @Override
    public int deleteStar(long givenBy, long givenTo) {
        return jdbcTemplate.update("DELETE FROM stars WHERE " +
                "given_by = ? AND given_to = ?", new Object[]{givenBy, givenTo});
    }





}
