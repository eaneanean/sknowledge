package com.example.demo.repository;

import com.example.demo.dto.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcTokenRepository implements TokenRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcTokenRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public int saveToken(Token token) {
        return jdbcTemplate.update("INSERT INTO token " +
                "(created_at, user_id, token) VALUES (?, ?, ?)",
                token.getCreatedAt(),
                token.getUserId(),
                token.getRandomString());
    }

    @Override
    public Token findToken(String token) {
        return jdbcTemplate.queryForObject("SELECT * " +
                "FROM token " +
                "WHERE token = ?", this::mapToken, token);
    }

    private Token mapToken(ResultSet resultSet, int i) throws SQLException {

        Token token = new Token();

        token.setCreatedAt(resultSet.getLong("created_at"));
        token.setRandomString(resultSet.getString("token"));
        token.setUserId(resultSet.getLong("user_id"));
        token.setTokenId(resultSet.getLong("id"));

        return token;
    }
}
