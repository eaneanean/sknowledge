package com.example.demo.repository;

import com.example.demo.dto.Location;
import com.example.demo.dto.User;
import com.example.demo.utils.LocaleChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Locale;

@Repository
public class JdbcUserRepository implements UserRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    GenderRepository genderRepository;

    Locale locale;

    @Autowired
    public JdbcUserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public User findUserById(long id, Locale locale) {
        if(jdbcTemplate == null) {
            throw new NullPointerException("jdbcTemplate seemes to be null" +
                    " in JdbcUserRepository");
        }


        this.locale = locale = LocaleChanger.changeLocale(locale);

        return jdbcTemplate.queryForObject("SELECT *" +
                        " FROM users" +
                        " WHERE id = ?",
                this::mapUser,
                id);

    }

    @Override
    public long saveUser(User user) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final String sqlExpression = "INSERT INTO users " +
                "(first_name, last_name, created_at, gender, age, email, " +
                "latitude, longitude, description, encrypted_password, enabled) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        jdbcTemplate.update( connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(sqlExpression,  Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getFirstName());
            ps.setString(2, user.getLastName());
            ps.setLong(3, user.getCreatedAt());
            ps.setInt(4, user.getGender().getId());
            ps.setInt(5, user.getAge());
            ps.setString(6, user.getEmail());
            ps.setDouble(7, user.getLocation().getLatitude());
            ps.setDouble(8, user.getLocation().getLongitude());
            ps.setString(9, user.getDescription());
            ps.setString(10, user.getEncryptedPassword());
            ps.setBoolean(11, user.getEnabled());

            return ps;
            }, keyHolder);

        return keyHolder.getKey().longValue();
    }


    /**
     *
     * @param email
     * @param locale might look that the method doesn't need it but it does.
     * @return
     */
    @Override
    public User findUserByEmail(String email, Locale locale) {

        this.locale = LocaleChanger.changeLocale(locale);

        RepositoryUtils.checkIfJdbcTemplateIsNull(jdbcTemplate,
                "jdbcTemplate is null" + " in findUserByEmail() method, class " +
                        this.getClass().getSimpleName().toString());

         List<User> userList = jdbcTemplate.query("SELECT *" +
                " FROM users" +
                " WHERE email = ?",
                this::mapUser,
                email);


         if(userList.isEmpty())
             return null;
         else if(userList.size() == 1)
             return userList.get(0);
         else
             throw new RuntimeException("This musn't happen. Email should be unique");

    }


    @Override
    public int updateUser(User user) {
        return jdbcTemplate.update("UPDATE users " +
                "SET first_name = ?, " +
                "last_name = ?, " +
                "gender = ?, " +
                "email = ?, " +
                "latitude = ?, " +
                "longitude = ?, " +
                "description = ?, " +
                "encrypted_password = ?, " +
                "enabled = ? " +
                        "WHERE id = ?",
                user.getFirstName(),
                user.getLastName(),
                user.getGender().getId(),
                user.getEmail(),
                user.getLocation().getLatitude(),
                user.getLocation().getLongitude(),
                user.getDescription(),
                user.getEncryptedPassword(),
                user.getEnabled(),
                user.getId());
    }


    private User mapUser(ResultSet resultSet, int i) throws SQLException {

        User user = new User();

        user.setId(resultSet.getLong("id"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setCreatedAt(resultSet.getLong("created_at"));
        user.setGender(genderRepository.findById(locale, resultSet.getInt("gender")));
        user.setEmail(resultSet.getString("email"));
        user.setLocation(new Location(resultSet.getDouble("latitude"),
                resultSet.getDouble("longitude")));
        user.setDescription(resultSet.getString("description"));
        user.setEncryptedPassword(resultSet.getString("encrypted_password"));
        user.setEnabled(resultSet.getBoolean("enabled"));

        return user;
    }


}
