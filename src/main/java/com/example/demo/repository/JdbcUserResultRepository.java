package com.example.demo.repository;

import com.example.demo.dto.Gender;
import com.example.demo.dto.InterestTeacher;
import com.example.demo.dto.UserResult;
import com.example.demo.utils.LocaleChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class JdbcUserResultRepository implements UserResultRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcUserResultRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }



    @Override
    public List<UserResult> findAll(long userId, Locale locale, String searchWord, String sorting) {

        locale = LocaleChanger.changeLocale(locale);

        System.out.println("UserId from JdbcUserresult:" + userId);
        SqlParameterSource parameterSource = new MapSqlParameterSource("myId", userId)
                .addValue("isoCode", locale.getLanguage());

        String sql = "SELECT *\n" +
                "FROM (\n" +
                "SELECT interests_teacher.user_id, interests_teacher.interest_id, gender, gender_translated, age, distance, \n" +
                "interests_teacher.level, name, first_name, last_name \n" +
                "FROM interests_learner, interests_teacher, users, gender_translation, interests_translation, (\n" +
                "SELECT id, distance\n" +
                "  FROM (\n" +
                " SELECT \n" +
                "        z.id,\n" +
                "        z.latitude, z.longitude,\n" +
                "        p.radius,\n" +
                "        p.distance_unit\n" +
                "                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))\n" +
                "                 * COS(RADIANS(z.latitude))\n" +
                "                 * COS(RADIANS(p.longpoint - z.longitude))\n" +
                "                 + SIN(RADIANS(p.latpoint))\n" +
                "                 * SIN(RADIANS(z.latitude)))) AS distance\n" +
                "  FROM users AS z\n" +
                "  JOIN (   /* these are the query parameters */\n" +
                "        SELECT  latitude  AS latpoint,  longitude AS longpoint,\n" +
                "                500.0 AS radius,      111.045 AS distance_unit\n" +
                "                FROM users where id = :myId\n" +
                "    ) AS p ON 1=1\n" +
                "  WHERE z.latitude\n" +
                "     BETWEEN p.latpoint  - (p.radius / p.distance_unit)\n" +
                "         AND p.latpoint  + (p.radius / p.distance_unit)\n" +
                "    AND z.longitude\n" +
                "     BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))\n" +
                "         AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))\n" +
                " ) AS d\n" +
                " WHERE distance <= radius AND d.id <> :myId\n" +
                ") as dq, (SELECT interests_learner.user_id as learnerId, min_level, interests_learner.min_language_level, interests_teacher.user_id as teacherId, level, \n" +
                "\t\t\t\t interests_learner.interest_id as interestid\n" +
                "FROM interests_learner, interests_teacher\n" +
                "WHERE interests_teacher.user_id = :myId \n" +
                "\tAND interests_learner.interest_id  = interests_teacher.interest_id\n" +
                "\t) as t1, ((\n" +
                "\t\tSELECT min_age, max_age, max_distance \n" +
                "\t\tFROM preferences \n" +
                "\t\tWHERE user_id = :myId\n" +
                "\t\tLIMIT 1) UNION (SELECT \"13\", \"100\", \"500\") LIMIT 1) as p1\n" +
                "WHERE interests_learner.user_id = :myId\n" +
                "\tAND age >= min_age \n" +
                "    AND age <= max_age \n" +
                "    AND distance <= max_distance \n" +
                "\tAND interests_teacher.user_id = users.id\n" +
                "\tAND users.gender = gender_translation.genderId \n" +
                "\tAND gender_translation.language_code = :isoCode\n" +
                " \tAND interests_learner.interest_id = interests_translation.interest_id \n" +
                " \tAND interests_translation.iso_code = :isoCode\n" +
                "\tAND interests_learner.interest_id = interests_teacher.interest_id\n" +
                "\tAND interests_learner.min_level <= interests_teacher.level \n" +
                "    AND dq.id = interests_teacher.user_id\n" +
                "    AND t1.learnerid = interests_teacher.user_id\n" +
                "    AND t1.min_level <= t1.level\n" +
                "\tAND EXISTS (SELECT * \n" +
                "\t    FROM users_languages \n" +
                "                WHERE user_id = :myId\n" +
                "\t \tAND iso_code in(SELECT iso_code \n" +
                "                                    FROM users_languages \n" +
                "                                    WHERE user_id = interests_teacher.user_id \n" +
                "                                    AND proficiency >= interests_learner.min_language_level\n" +
                "                                    )\n" +
                "                )\n" +
                "    AND EXISTS (SELECT *\n" +
                "    \tFROM users_languages \n" +
                "    \t\t\tWHERE user_id = t1.learnerid\n" +
                "\t\t\t\t \tAND iso_code in(SELECT iso_code \n" +
                "                                FROM users_languages \n" +
                "                                WHERE user_id = :myId\n" +
                "                                AND proficiency >= t1.min_language_level\n" +
                "                                ) \n" +
                "    )     \n" +
                ") as t1 LEFT JOIN star_user\n" +
                "ON t1.user_id = star_user.given_to\n" +
                "WHERE CONCAT_WS(\" \", first_name, last_name) LIKE \"%" + searchWord + "%\"\n" +
                "ORDER BY " + sorting;


        return jdbcTemplate.query(sql, parameterSource, (ResultSet rs) -> {

            Map<Long, UserResult> users = new LinkedHashMap<>();

            while(rs.next()) {

                long id = rs.getLong("user_id");

                boolean isAlreadySeen = users.containsKey(id);
                UserResult userResult = new UserResult();

                if(!isAlreadySeen) {
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");

                    int genderId = rs.getInt("gender");
                    String genderTranslated = rs.getString("gender_translated");
                    Gender gender = new Gender(genderId, genderTranslated);

                    int age = rs.getInt("age");
                    double distance = rs.getDouble("distance");

                    int interestId = rs.getInt("interest_id");
                    int level = rs.getInt("level");
                    String interestName = rs.getString("name");

                    double average = rs.getDouble("average");
                    int total = rs.getInt("total");

                    userResult.setAge(age);
                    userResult.setFirstName(firstName);
                    userResult.setLastName(lastName);
                    userResult.setGender(gender);
                    userResult.setAverageStars(average);
                    userResult.setNumberOfPeopleWhoRatedTheUser(total);
                    userResult.setDistance(distance);
                    userResult.setId(id);

                    users.put(userResult.getId(), userResult);
                } else {
                    userResult = users.get(id);
                }

                int interestId = rs.getInt("interest_id");
                int level = rs.getInt("level");
                String interestName = rs.getString("name");

                InterestTeacher interestTeacher = new InterestTeacher(interestName,
                        interestId,
                        level);

                userResult.addInterestTeacher(interestTeacher);

            }
            return users.values().stream().collect(Collectors.toList());
        });






    }
}
