package com.example.demo.repository;

import com.example.demo.dto.Language;
import com.example.demo.dto.UserLanguage;

import java.util.List;
import java.util.Locale;

public interface LanguageRepository {

    public Language findOne(String isoCode);

    public List<Language> findAll(Locale locale);

    public List<UserLanguage> findForUser(long userId, Locale locale);

    public int deleteAllInterestForUser(long userId);

    int [] batchInsertLanguagesForUser(long userId, List<UserLanguage> list);
}
