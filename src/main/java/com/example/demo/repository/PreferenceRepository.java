package com.example.demo.repository;

import com.example.demo.dto.Preference;
import org.springframework.stereotype.Repository;

@Repository
public interface PreferenceRepository {

    /**
     *
     * @param id
     * @return either the preference for the given user or null if non existent
     */
    public Preference findPreference(long id);

    public int insertPreference(long id, Preference preference);

}
