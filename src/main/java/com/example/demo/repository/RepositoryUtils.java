package com.example.demo.repository;

import org.springframework.jdbc.core.JdbcTemplate;

public class RepositoryUtils {

    public static void checkIfJdbcTemplateIsNull (JdbcTemplate jdbcTemplate, String errorMessage)
            throws NullPointerException {
            if(jdbcTemplate == null)
                throw new NullPointerException(errorMessage);
    }

}
