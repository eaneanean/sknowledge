package com.example.demo.repository;

import com.example.demo.dto.Star;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public interface StarsRepository {
    public Integer findStar(long fromId, long toId);
    public int saveOrReplace(Star star);
    public int deleteStar(long givenBy, long givenTo);
}
