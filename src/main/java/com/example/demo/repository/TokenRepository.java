package com.example.demo.repository;

import com.example.demo.dto.Token;

public interface TokenRepository {

    public int saveToken(Token token);
    public Token findToken(String token);
}
