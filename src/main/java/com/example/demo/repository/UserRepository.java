package com.example.demo.repository;

import com.example.demo.dto.User;

import java.util.Locale;

public interface UserRepository {

    public User findUserById(long id, Locale locale);

    /**
     *
     * @param user user to be inserted
     * @return the id the user(not column index, but id of the user)
     */
    public long saveUser(User user);

    public User findUserByEmail(String email, Locale locale);

    public int updateUser(User user);

}
