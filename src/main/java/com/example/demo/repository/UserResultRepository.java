package com.example.demo.repository;

import com.example.demo.dto.UserResult;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;

@Repository
public interface UserResultRepository {

    public List<UserResult> findAll(long userId, Locale locale, String searchWord, String order);

}
