package com.example.demo.service;

import com.example.demo.dto.User;
import com.example.demo.dto.Token;
import com.example.demo.utils.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

@Service
public class ConformationService {

    @Autowired
    private EmailService emailService;

    @Autowired
    MessageSource messageSource;


    public void sendConformationMail(User user, Token token, String url) {
        Messages messages = new Messages(messageSource, LocaleContextHolder.getLocale());
        emailService.sendSimpleMessage(user.getEmail(),
                messages.getValue("email.conformation.title"),
                messages.getValue("email.conformation") + url + "/" + token.getRandomString());
    }

    public void sendChangePassword(String email, Token token, String url) {
        Messages messages = new Messages(messageSource, LocaleContextHolder.getLocale());
        emailService.sendSimpleMessage(email,
                messages.getValue("email.password.title"),
                messages.getValue("email.password") + url + "/" + token.getRandomString());
    }

    public String getURI(HttpServletRequest request, String path) throws MalformedURLException, URISyntaxException {
        URL url = new URL(request.getRequestURL().toString());
        String host  = url.getHost();
        String userInfo = url.getUserInfo();
        String scheme = url.getProtocol();
        int port = url.getPort();
        URI uri = new URI(scheme,userInfo,host,port,path,null,null);
        return uri.toString();
    }


}
