package com.example.demo.service;

import com.example.demo.dto.ContactForm;
import org.springframework.stereotype.Service;

@Service
public interface ContactService {

    long saveContactService(ContactForm contactForm);
}
