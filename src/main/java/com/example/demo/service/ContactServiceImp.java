package com.example.demo.service;


import com.example.demo.dto.ContactForm;
import com.example.demo.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactServiceImp implements ContactService {

    @Autowired
    ContactRepository contactRepository;

    @Override
    public long saveContactService(ContactForm contactForm) {
        return contactRepository.saveContact(contactForm);
    }
}
