package com.example.demo.service;

import com.example.demo.dto.DetailedUserResult;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public interface DetailedUserResultService {

    public DetailedUserResult findById(long id, Locale locale);

}
