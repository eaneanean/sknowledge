package com.example.demo.service;

import com.example.demo.dto.DetailedUserResult;
import com.example.demo.repository.DetailedUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class DetailedUserResultServiceImp implements DetailedUserResultService {

    @Autowired
    DetailedUserRepository repository;


    @Override
    public DetailedUserResult findById(long id, Locale locale) {
        return repository.findDetailedUserResultById(id, locale);
    }
}
