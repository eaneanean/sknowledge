package com.example.demo.service;

public interface EmailService {
    void sendSimpleMessage(String sendTo, String subject, String text);
}
