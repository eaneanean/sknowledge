package com.example.demo.service;

import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Map;

@Service
public interface ErrorService {

    /**
     * Gets the JavaScript error messages
     * @param locale
     * @return
     */
    public Map<String, String> getErrorMessages(Locale locale);


}
