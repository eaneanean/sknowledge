package com.example.demo.service;

import com.example.demo.utils.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service
public class ErrorServiceImp implements ErrorService{

    @Autowired
    MessageSource messageSource;

    @Override
    public Map<String, String> getErrorMessages(Locale locale) {
        Messages messages = new Messages(messageSource, locale);
        Map<String, String> map = new HashMap<>();
        map.put("javascript.latitude", messages.getValue("javascript.latitude"));
        map.put("javascript.longitude", messages.getValue("javascript.longitude"));
        map.put("javascript.userDeniedAccess", messages.getValue("javascript.userDeniedAccess"));

        return map;
    }
}
