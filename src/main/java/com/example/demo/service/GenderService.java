package com.example.demo.service;

import com.example.demo.dto.Gender;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public interface GenderService {

    public Iterable<Gender> findAll(Locale locale);
    public Gender findById(Locale locale, int id);
}
