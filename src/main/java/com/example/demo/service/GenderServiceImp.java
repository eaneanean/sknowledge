package com.example.demo.service;

import com.example.demo.dto.Gender;
import com.example.demo.repository.GenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class GenderServiceImp implements GenderService {

    @Autowired
    GenderRepository genderRepository;


    @Override
    public Iterable<Gender> findAll(Locale locale) {
        return genderRepository.findAll(locale);
    }

    @Override
    public Gender findById(Locale locale, int id) {
        return genderRepository.findById(locale, id);
    }
}
