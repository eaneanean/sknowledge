package com.example.demo.service;

import com.example.demo.dto.InterestLearner;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public interface InterestLearnerService {

    List<InterestLearner> findInterestsByUserId(long userId, Locale locale);

    /**
     * First deletes every user learner for the given user, then replaces it with the
     * new ones provided as list.
     * @param userId
     * @param list
     */
    public void replaceInterestLearner (long userId, List<InterestLearner> list);

}
