package com.example.demo.service;

import com.example.demo.dto.InterestLearner;
import com.example.demo.repository.InterestLearnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class InterestLearnerServiceImp implements InterestLearnerService {

    @Autowired
    InterestLearnerRepository repository;


    @Override
    public List<InterestLearner> findInterestsByUserId(long userId, Locale locale) {
        return repository.findInterestsByUserId(userId, locale);
    }

    @Override
    public void replaceInterestLearner (long userId, List<InterestLearner> list) {
        repository.deleteAllInterestForUser(userId);
        repository.batchInsertInterestLearner(userId,
                deleteIfNonExistentInterest(list));
    }

    /**
     *
     * @param allInterests
     * @return it returns the list, so only the elements with id value different
     *      *             from -1(the value chosen for nonexistent element) are left
     */
    private List<InterestLearner> deleteIfNonExistentInterest(List<InterestLearner> allInterests) {

        List<InterestLearner> validInterest = new ArrayList<>();

        for(int i = 0; i < allInterests.size(); i++) {
            if(allInterests.get(i).getInterestId() != -1)
                validInterest.add(allInterests.get(i));
        }

        return validInterest;
    }




}
