package com.example.demo.service;

import com.example.demo.dto.Interest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public interface InterestService {

    List<Interest> findAllInterests(Locale locale);


}
