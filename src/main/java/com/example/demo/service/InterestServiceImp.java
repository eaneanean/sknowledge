package com.example.demo.service;

import com.example.demo.dto.Interest;
import com.example.demo.repository.InterestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class InterestServiceImp implements InterestService {

    @Autowired
    InterestRepository repository;

    @Override
    public List<Interest> findAllInterests(Locale locale) {
        return repository.findAllInterests(locale);
    }
}
