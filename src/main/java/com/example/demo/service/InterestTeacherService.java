package com.example.demo.service;

import com.example.demo.dto.InterestTeacher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public interface InterestTeacherService {

    List<InterestTeacher> findInterestsByUserId(long userId, Locale locale);

    /**
     * First deletes every user teacher for the given user, then replaces it with the
     * new ones provided as list.
     * @param userId
     * @param list
     */
    public void replaceInterestTeacher (long userId, List<InterestTeacher> list);


}
