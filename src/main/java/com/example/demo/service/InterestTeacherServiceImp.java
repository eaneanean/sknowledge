package com.example.demo.service;

import com.example.demo.dto.InterestTeacher;
import com.example.demo.repository.InterestTeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class InterestTeacherServiceImp implements InterestTeacherService {

    @Autowired
    InterestTeacherRepository interestTeacherRepository;

    @Override
    public List<InterestTeacher> findInterestsByUserId(long userId, Locale locale) {
        return interestTeacherRepository.findInterestsByUserId(userId, locale);
    }

    @Override
    public void replaceInterestTeacher(long userId, List<InterestTeacher> list) {
            interestTeacherRepository.deleteAllInterestForUser(userId);
            interestTeacherRepository.batchInsertInterestTeacher(userId,
                    deleteIfNonExistentInterest(list));
    }


    /**
     *
     * @param allInterests
     * @return it returns the list, so only the elements with id value different
     *      *             from -1(the value chosen for nonexistent element) are left
     */
    private List<InterestTeacher> deleteIfNonExistentInterest(List<InterestTeacher> allInterests) {

        List<InterestTeacher> validInterest = new ArrayList<>();

        for(int i = 0; i < allInterests.size(); i++) {
            if(allInterests.get(i).getInterestId() != -1)
                validInterest.add(allInterests.get(i));
        }

        return validInterest;
    }
}
