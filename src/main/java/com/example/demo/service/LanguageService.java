package com.example.demo.service;

import com.example.demo.dto.Language;
import com.example.demo.dto.UserLanguage;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public interface LanguageService {

    List<UserLanguage> findLanguagesByUserId(long userId, Locale locale);

    /**
     * First deletes every language for the given user, then replaces it with the
     * new ones provided as list.
     * @param userId
     * @param list
     */
    public void replaceLanguages (long userId, List<UserLanguage> list);

    public List<Language> findAll(Locale locale);

    public List<UserLanguage> findForUser(long userId, Locale locale);
}
