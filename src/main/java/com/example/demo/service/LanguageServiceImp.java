package com.example.demo.service;

import com.example.demo.dto.Language;
import com.example.demo.dto.UserLanguage;
import com.example.demo.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class LanguageServiceImp implements LanguageService {

    @Autowired
    LanguageRepository languageRepository;

    @Override
    public List<UserLanguage> findLanguagesByUserId(long userId, Locale locale) {
        return languageRepository.findForUser(userId, locale);
    }

    @Override
    public void replaceLanguages(long userId, List<UserLanguage> list) {
        languageRepository.deleteAllInterestForUser(userId);
        languageRepository.batchInsertLanguagesForUser(userId, deleteIfNonExistentLanguage(list));

    }

    /**
     *
     * @param allLanguages doesn't change the list
     * @return all the languages which don't have value of -1(which is invalid value)
     */
    private List<UserLanguage> deleteIfNonExistentLanguage(List<UserLanguage> allLanguages) {
        List<UserLanguage> validLanguages = new ArrayList<>();

        for(int i = 0; i < allLanguages.size(); i++) {
            if(!allLanguages.get(i).getCode().equals("-1") &&
                    !(allLanguages.get(i).getProficiency() == -1))
                validLanguages.add(allLanguages.get(i));
        }

        return validLanguages;
    }

    public List<Language> findAll(Locale locale) {
        return languageRepository.findAll(locale);
    }

    public List<UserLanguage> findForUser(long userId, Locale locale) {
        return languageRepository.findForUser(userId, locale);
    }
}
