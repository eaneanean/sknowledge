package com.example.demo.service;

import com.example.demo.dto.Preference;
import org.springframework.stereotype.Service;

@Service
public interface PreferenceService {

    public Preference findPreference(long id);

    public int insertPreference(long id, Preference preference);
}
