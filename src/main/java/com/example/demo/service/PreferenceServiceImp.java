package com.example.demo.service;

import com.example.demo.dto.Preference;
import com.example.demo.repository.PreferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PreferenceServiceImp implements PreferenceService {

    @Autowired
    PreferenceRepository preferenceRepository;


    @Override
    public Preference findPreference(long id) {
        return preferenceRepository.findPreference(id);
    }

    @Override
    public int insertPreference(long id, Preference preference) {
        return preferenceRepository.insertPreference(id, preference);
    }
}
