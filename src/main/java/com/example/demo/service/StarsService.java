package com.example.demo.service;

import com.example.demo.dto.Star;
import org.springframework.stereotype.Service;

@Service
public interface StarsService {

    /**
     *
     * @param fromId
     * @param toId
     * @return null if no row is found, otherwise the star given
     */
    public Integer findStar(long fromId, long toId);
    public int saveOrReplace(Star star);
}
