package com.example.demo.service;

import com.example.demo.dto.Star;
import com.example.demo.repository.StarsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class StarsServiceImp implements StarsService {

    @Autowired
    StarsRepository starsRepository;

    @Override
    public Integer findStar(long fromId, long toId) {
        return starsRepository.findStar(fromId, toId);
    }

    public int saveOrReplace(Star star) {
        // it means that it should be deleted
        if(star.getStars() == -1)
            return starsRepository.deleteStar(star.getGivenBy(), star.getGivenTo());
        else
            return starsRepository.saveOrReplace(star);
    }
}
