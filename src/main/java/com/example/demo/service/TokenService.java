package com.example.demo.service;

import com.example.demo.dto.User;
import com.example.demo.dto.Token;


public interface TokenService {

    public Token createToken(User user);
    public int saveToken(Token token);
    public Token findTokenByTokenText(String token);
}
