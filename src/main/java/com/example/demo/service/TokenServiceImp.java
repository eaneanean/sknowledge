package com.example.demo.service;

import com.example.demo.dto.User;
import com.example.demo.dto.Token;
import com.example.demo.repository.TokenRepository;
import com.example.demo.utils.RandomGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class TokenServiceImp implements TokenService {

    RandomGenerator randomGenerator = new RandomGenerator();

    @Autowired
    TokenRepository tokenRepository;

    @Override
    public Token createToken(User user) {
        Token token = new Token();
        token.setCreatedAt(Instant.now().toEpochMilli());
        token.setUserId(user.getId());


        try {
            token.setRandomString(randomGenerator.generateRandomBytes(60));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }

    @Override
    public int saveToken(Token token) {
        return tokenRepository.saveToken(token);
    }

    @Override
    public Token findTokenByTokenText(String token) {
        return tokenRepository.findToken(token);
    }
}
