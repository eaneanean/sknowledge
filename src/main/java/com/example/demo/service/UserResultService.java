package com.example.demo.service;

import com.example.demo.dto.UserResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public interface UserResultService {

    public List<UserResult> findAll(long userId, int pageNumber, Locale locale, String searchWord, String order);
}
