package com.example.demo.service;

import com.example.demo.dto.UserResult;
import com.example.demo.repository.UserResultRepository;
import com.example.demo.utils.UserResultSorting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class UserResultServiceImp implements UserResultService {

    @Autowired
    UserResultRepository userResultRepository;

    @Override
    public List<UserResult> findAll(long userId, int pageNumber, Locale locale, String searchWord, String order) {

        String sqlSortCommand = UserResultSorting.requestParameterToSql(order);
        System.out.println(sqlSortCommand);

        List<UserResult> list =  userResultRepository.findAll(userId, locale, searchWord, sqlSortCommand);
        Pageable pageable= PageRequest.of(pageNumber - 1, 2);

        System.out.println("SIZE:" + list.size());

        PageImpl<UserResult> page = new PageImpl(list, pageable, list.size());


        PagedListHolder<UserResult> holder = new PagedListHolder<>(list);
        holder.setPageSize(2);
        holder.setPage(pageNumber - 1);


        return list;



    }
}
