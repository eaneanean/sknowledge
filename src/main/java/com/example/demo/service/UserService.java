package com.example.demo.service;

import com.example.demo.dto.User;
import com.example.demo.dto.RegisterForm;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Locale;

@Service
public interface UserService {

    public void saveProfilePicture(MultipartFile profilePicture, long userId) throws IOException;

    byte[] readSmallImage(long UserId) throws IOException;

    public boolean emailAlreadyUsed(String email, Locale locale);
    public User formToUser(User user, RegisterForm form);

    /**
     *
     * @param user user to be saved
     * @return id of the user
     */
    public long saveUser(User user);

    public User findUserById(long id, Locale locale);

    public int updateUser(User user);

    public User findUserByMail(String mail, Locale locale);

    public byte [] readBigImage(long userId) throws IOException;

    public String readbase64OfTheBigPicture(long userId) throws IOException;

    public String readbase64OfSmallPicture(long userId) throws IOException;

    public String readBaseOfBigPictureOrDefault(long userId, int genderId) throws Exception;
    public String readBaseOfSmallPictureOrDefault(long userId, int genderId) throws Exception;
}
