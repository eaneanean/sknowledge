package com.example.demo.service;


import com.google.cloud.ReadChannel;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.google.cloud.storage.Storage;

@Service
public class UserServiceCloudImp extends UserServiceImp {

    final String BUCKET = "shareknowledge-233.appspot.com";

    @Autowired
    Storage storage;

    @Value("${smallImages.height}")
    String smallImagesHeight;




    @Override
    public void saveProfilePicture(MultipartFile profilePicture, long userId) throws IOException {

        byte[] bytes = profilePicture.getBytes();

        BlobId blobId = BlobId.of(BUCKET, String.valueOf(userId) + ".jpg");
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

        storage.create(blobInfo, bytes);

        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage bImageFromConvert = ImageIO.read(in);

        saveSmallProfilePicture(bImageFromConvert, userId);

    }

    @Override
    public void saveSmallProfilePicture(BufferedImage img, long userId) throws IOException {

        Image smallImage = img.getScaledInstance(-1, Integer.valueOf(smallImagesHeight), Image.SCALE_DEFAULT);
        BufferedImage bufferedImage = new BufferedImage(smallImage.getWidth(null), smallImage.getHeight(null), BufferedImage.TYPE_INT_RGB);
        bufferedImage.getGraphics().drawImage(smallImage, 0, 0 , null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "jpg", baos);
        byte [] imageAsBytes = baos.toByteArray();

        BlobId blobId = BlobId.of(BUCKET, String.valueOf(userId) + "_small.jpg");
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

        storage.create(blobInfo, imageAsBytes);


        bufferedImage.getGraphics().dispose();
    }

    @Override
    public String readbase64OfTheBigPicture(long userId) throws IOException {
        return Base64.getEncoder().encodeToString(readBigImage(userId));
    }

    @Override
    public String readbase64OfSmallPicture(long userId) throws IOException {
        return Base64.getEncoder().encodeToString(readSmallImage(userId));
    }

    @Override
    public byte [] readBigImage(long userId) throws IOException {
        return readImage(String.valueOf(userId) + ".jpg");
    }

    @Override
    public byte [] readSmallImage(long userId) throws IOException {
        return readImage(String.valueOf(userId) + "_small.jpg");
    }

    private byte [] readImage(String file) throws IOException {
        List<Byte> pictureInBytes = new ArrayList<>();

        try (ReadChannel channel = storage.reader(BUCKET, file)) {
            ByteBuffer bytes = ByteBuffer.allocate(128 * 1024);
            while (channel.read(bytes) > 0) {
                bytes.flip();
                while(bytes.hasRemaining()){
                    pictureInBytes.add(bytes.get());
                }
                bytes.clear();
            }
        }
        catch (Exception e) {
            throw new IOException();
        }

        byte [] pictureInBytesArray = new byte[pictureInBytes.size()];

        for(int i = 0; i < pictureInBytes.size(); i++)
            pictureInBytesArray[i] = pictureInBytes.get(i);

        return pictureInBytesArray;
    }


    @Override
    public String readBaseOfBigPictureOrDefault(long userId, int genderId) throws Exception {
        try {
            String bigImage = readbase64OfTheBigPicture(userId);
            return bigImage;
        } catch (Exception e) {
            System.out.println(e);
            if(genderId == 0) {
                // -1 is the picture for male
                return readbase64OfTheBigPicture(-1);
            }
            else if(genderId == 1) {
                // -2 is the picture for female
                return readbase64OfTheBigPicture(-2);
            }
            else if(genderId == 2) {
                return readbase64OfTheBigPicture(-4);
            }
            else if(genderId == 3) {
                return readbase64OfTheBigPicture(-3);
            }
            else {
                throw new Exception();
            }
        }
    }

    @Override
    public String readBaseOfSmallPictureOrDefault(long userId, int genderId) throws Exception {
        try {
            String smallImage = readbase64OfSmallPicture(userId);
            return smallImage;
        } catch (Exception e) {
            System.out.println(e);
            if(genderId == 0) {
                // -1 is the picture for male
                return readbase64OfSmallPicture(-1);
            }
            else if(genderId == 1) {
                // -2 is the picture for female
                return readbase64OfSmallPicture(-2);
            }
            else if(genderId == 2) {
                return readbase64OfSmallPicture(-4);
            }
            else if(genderId == 3) {
                return readbase64OfSmallPicture(-3);
            }
            else {
                throw new Exception();
            }
        }
    }
}
