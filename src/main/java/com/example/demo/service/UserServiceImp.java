package com.example.demo.service;

import com.example.demo.dto.Location;
import com.example.demo.dto.User;
import com.example.demo.dto.RegisterForm;
import com.example.demo.repository.GenderRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.Instant;
import java.util.Locale;

import java.util.Base64;


public class UserServiceImp implements UserService {

    @Autowired
    GenderRepository genderRepository;

    @Autowired
    @Qualifier("BCrypt")
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;

    @Value("${images.path}")
    String originalImagesFolder;

    @Value("${smallImages.path}")
    String smallImagesFolder;

    @Value("${smallImages.height}")
    String smallImagesHeight;


    @Override
    public void saveProfilePicture(MultipartFile profilePicture, long userId) throws IOException {
        System.out.println("saveProfilePicture from UserServiceImp is called");
        System.out.println("Resource type:" + profilePicture.getContentType());
        byte[] bytes = profilePicture.getBytes();

        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage bImageFromConvert = ImageIO.read(in);

        saveSmallProfilePicture(bImageFromConvert, userId);

        ImageIO.write(bImageFromConvert, "jpg", new File(originalImagesFolder + userId + ".jpg"));
    }

    public void saveSmallProfilePicture(BufferedImage img, long userId) throws IOException  {

        Image smallImage = img.getScaledInstance(-1, Integer.valueOf(smallImagesHeight), Image.SCALE_DEFAULT);
        BufferedImage bufferedImage = new BufferedImage(smallImage.getWidth(null), smallImage.getHeight(null), BufferedImage.TYPE_INT_RGB);
        bufferedImage.getGraphics().drawImage(smallImage, 0, 0 , null);

        try {
            ImageIO.write(bufferedImage, "jpg", new File(smallImagesFolder + userId + ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        bufferedImage.getGraphics().dispose();
    }

    public String readbase64OfTheBigPicture(long userId) throws IOException {
        return Base64.getEncoder().encodeToString(readBigImage(userId));
    }

    public String readbase64OfSmallPicture(long userId) throws IOException {
        return Base64.getEncoder().encodeToString(readSmallImage(userId));
    }

    @Override
    public String readBaseOfSmallPictureOrDefault(long userId, int genderId) throws Exception {
        try {
            String smallImage = readbase64OfSmallPicture(userId);
            return smallImage;
        } catch (IOException e) {
            if(genderId == 0) {
                // -1 is the picture for male
                return readbase64OfSmallPicture(-1);
            }
            else if(genderId == 1) {
                // -2 is the picture for female
                return readbase64OfSmallPicture(-2);
            }
            else if(genderId == 2) {
                return readbase64OfSmallPicture(-4);
            }
            else if(genderId == 3) {
                return readbase64OfSmallPicture(-3);
            }
            else {
                throw new Exception();
            }
        }
    }

    @Override
    public String readBaseOfBigPictureOrDefault(long userId, int genderId) throws Exception {
        try {
            String bigImage = readbase64OfTheBigPicture(userId);
            return bigImage;
        } catch (IOException e) {
            if(genderId == 0) {
                // -1 is the picture for male
                return readbase64OfTheBigPicture(-1);
            }
            else if(genderId == 1) {
                // -2 is the picture for femail
                return readbase64OfTheBigPicture(-2);
            }
            else if(genderId == 2) {
                return readbase64OfTheBigPicture(-4);
            }
            else if(genderId == 3) {
                return readbase64OfTheBigPicture(-3);
            }
            else {
                throw new Exception();
            }
        }
    }


    @Override
    public byte [] readBigImage(long userId) throws IOException {

        File file = new File(originalImagesFolder + userId + ".jpg");
        InputStream input = new FileInputStream(file);
        byte [] bytes = input.readAllBytes();;

        input.close();

        return bytes;
    }

    @Override
    public byte[] readSmallImage(long userId) throws IOException {

        File file = new File(smallImagesFolder + userId + ".jpg");

        InputStream input = new FileInputStream(file);
        byte [] bytes = input.readAllBytes();;

        input.close();

        return bytes;

    }



    @Override
    public boolean emailAlreadyUsed(String email, Locale locale) {

     return !(userRepository.findUserByEmail(email, locale) == null);
    }

    @Override
    public User formToUser(User user, RegisterForm form) {

        user.setFirstName(form.getFirstName());
        user.setLastName(form.getLastName());
        user.setAge(form.getAge());
        user.setGender(genderRepository.findById(LocaleContextHolder.getLocale(), form.getGender()));
        user.setEmail(form.getEmail());
        user.setLocation(new Location(form.getLatitude(), form.getLongitude()));
        user.setCreatedAt(Instant.now().toEpochMilli());
        user.setEncryptedPassword(passwordEncoder.encode(form.getUnencryptedPassword()));
        user.setEnabled(false);

        return user;
    }

    @Override
    public long saveUser(User user) {
        long userId = userRepository.saveUser(user);
        user.setId(userId);
        return userId;
    }

    @Override
    public User findUserById(long id, Locale locale) {
        return userRepository.findUserById(id, locale);
    }

    @Override
    public int updateUser(User user) {
        return userRepository.updateUser(user);
    }

    @Override
    public User findUserByMail(String mail, Locale locale) {
        return userRepository.findUserByEmail(mail, locale);
    }


}
