package com.example.demo.utils;

import org.springframework.context.support.ResourceBundleMessageSource;

import java.io.StringBufferInputStream;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AllMessagesResourceBundleMessageSource
        extends ResourceBundleMessageSource {

    public Map<String,String> getKeys(String basename, Locale locale, Predicate<String> predicate) {
        ResourceBundle resourceBundle = getResourceBundle(basename, locale);
        Set<String> messages = resourceBundle.keySet();
        Set<String> filteredMessages = messages.stream().filter(predicate).collect(Collectors.toSet());
        Map<String, String> map = new HashMap<>();

        map = filteredMessages.stream().collect(Collectors.toMap(Function.identity(), x -> getStringOrNull(resourceBundle, x)));

        return map;
    }


}


