package com.example.demo.utils;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Component
@PropertySource("classpath:messages/messages_mk.properties")
@ConfigurationProperties(prefix = "")
public class JavaScriptPropertiesService {

    private Map<String, String> javascript = new HashMap<>();

    public Map<String, String> getJavascript() {
        return javascript;
    }

    public void setJavascript(Map<String, String> map) {
        this.javascript = map;
    }

    public void print() {
        javascript.forEach((k, v) -> System.out.println(k + "," + v));
    }
}
