package com.example.demo.utils;

import java.util.Locale;

public class LocaleChanger {


    /**
     * Most of the database works by getting results in which the locale, that is
     * the language part of the locale plays necessary part. If this method is not used,
     * when the user has browser preferences which are different than Macedonian or English,
     * he won't get any results when browsing. Now he will get the result in English
     *
     * This method must be UPDATED when new language support is added.
     * @param locale
     * @return
     */
    public static Locale changeLocale(Locale locale) {

        if(!locale.getLanguage().equals("mk") &&
                !locale.getLanguage().equals("en"))
            return new Locale("en");
        else
            return locale;
    }

}
