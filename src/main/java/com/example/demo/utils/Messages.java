package com.example.demo.utils;

import com.example.demo.configuration.MessageConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Locale;

public class Messages {

    private MessageSource messageSource;
    private MessageSourceAccessor messageSourceAccessor;
    private Locale locale;

    public Messages(MessageSource messageSource, Locale locale) {
        this.locale = locale;
        this.messageSource = messageSource;
        System.out.println("Messages locale:" + LocaleContextHolder.getLocale());
        messageSourceAccessor = new MessageSourceAccessor(messageSource, locale);
    }

    public String getValue(String key) {
        return messageSourceAccessor.getMessage(key);
    }


}


