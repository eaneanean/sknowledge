package com.example.demo.utils;

import java.util.HashMap;
import java.util.Map;

public class UserResultSorting {

    private static final String FIRST_NAME_LAST_ASCENDING = "first_name ASC, last_name ASC";
    private static final String FIRST_NAME_LAST_DESCENDING = "first_name DESC, last_name DESC";
    private static final String STAR_ASCENDING = "average ASC";
    private static final String STAR_DESCENDING = "average DESC";
    private static final String DISTANCE_CLOSER_FIRST = "distance ASC";
    private static final String DISTANCE_CLOSER_LAST = "distance DESC";

    public static String requestParameterToSql(String param) {

        Map<String, String> map = new HashMap<>();
        map.put("dist_asc", DISTANCE_CLOSER_FIRST);
        map.put("dist_desc", DISTANCE_CLOSER_LAST);
        map.put("star_asc", STAR_ASCENDING);
        map.put("star_desc", STAR_DESCENDING);
        map.put("name_asc", FIRST_NAME_LAST_ASCENDING);
        map.put("name_desc", FIRST_NAME_LAST_DESCENDING);

        return map.get(param);
    }



}
