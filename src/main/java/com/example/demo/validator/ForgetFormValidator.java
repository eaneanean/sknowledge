package com.example.demo.validator;

import com.example.demo.dto.ForgetForm;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Locale;

@Component
public class ForgetFormValidator implements Validator {

    @Autowired
    UserService userService;

    private Locale locale;

    @Override
    public boolean supports(Class<?> aClass) {
         return ForgetForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ForgetForm forgetForm = (ForgetForm) o;

        if (!userService.emailAlreadyUsed(forgetForm.getEmail(), locale )) {
            errors.rejectValue("email", "notRegisteredEmail.error");
        }

    }

    public void validateWithLocale(Object o, Errors errors, Locale locale) {
        this.locale = locale;
        validate(o, errors);
    }
}
