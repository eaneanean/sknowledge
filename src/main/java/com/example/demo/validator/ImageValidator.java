package com.example.demo.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URLConnection;
import java.util.Arrays;

public class ImageValidator implements Validator {

    final String [] acceptableTypes = new String[]{"image/png", "image/jpeg", "image/gif"};
    final int allowedImageSizeInMb = 5;

    @Override
    public boolean supports(Class<?> aClass) {
        return (new byte[1]).getClass().equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        byte [] bytes = (byte []) o;

        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        String type = "";
        try {
            type = URLConnection.guessContentTypeFromStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        boolean typeIsValid = Arrays.stream(acceptableTypes).anyMatch(type::equals);

        if(!typeIsValid)
            errors.rejectValue("profilePicture","error.imageFormat");

    }
}
