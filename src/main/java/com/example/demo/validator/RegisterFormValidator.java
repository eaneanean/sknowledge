package com.example.demo.validator;

import com.example.demo.dto.RegisterForm;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Locale;

@Component
public class RegisterFormValidator implements Validator {

        @Autowired
        UserService userService;

        Locale locale;

        @Override
        public boolean supports(Class<?> aClass) {
        return RegisterForm.class.equals(aClass);
    }

        @Override
        public void validate(Object o, Errors errors) {

            RegisterForm registerForm = (RegisterForm) o;

            if (userService == null) {
                System.out.println("Userservice is nulll");
            } else {
                System.out.println("Userservice is not nulll");
            }


            if (!registerForm.getUnencryptedPassword().equals(registerForm.getUnencryptedRepeatedPassword())) {
                errors.rejectValue("unencryptedRepeatedPassword", "error.differentPasswords");
            }

            if(registerForm.getUnencryptedPassword().length() < 8) {
                errors.rejectValue("unencryptedPassword", "error.register.pass");
            }


            if (userService.emailAlreadyUsed(registerForm.getEmail(), locale )) {
                errors.rejectValue("email", "register.alreadyInUseEmail.error");
            }

            if(registerForm.getEmail() == null || registerForm.getEmail().isEmpty()) {
                errors.rejectValue("email", "register.email.error");
            }

        }

        public void validateWithLocale(Object o, Errors errors, Locale locale) {
            this.locale = locale;
            validate(o, errors);
        }

}
