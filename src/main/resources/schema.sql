--DROP TABLE IF EXISTS languages_translation;
--DROP TABLE IF EXISTS gender_translation;
--DROP TABLE IF EXISTS interests_learner;
--DROP TABLE IF EXISTS interests_teacher;
--DROP TABLE IF EXISTS interests_translation;
--DROP TABLE IF EXISTS users_languages;
--DROP TABLE IF EXISTS users_pictures;
--DROP VIEW IF EXISTS star_user;
--DROP VIEW IF EXISTS user_view;
--DROP TABLE IF EXISTS interests;
--DROP TABLE IF EXISTS stars;
--DROP TABLE IF EXISTS token;
--DROP TABLE IF EXISTS users;
--DROP TABLE IF EXISTS languages;
--DROP TABLE IF EXISTS gender;


CREATE TABLE IF NOT EXISTS languages (
    iso_code CHAR(2) NOT NULL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS languages_translation (
    id INT AUTO_INCREMENT PRIMARY KEY,
    iso_code CHAR(2),
    language_code CHAR(2),
    language_translated VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS gender (
    id INT PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS gender_translation (
    id INT AUTO_INCREMENT PRIMARY KEY,
    genderId INT,
    language_code CHAR(2),
    gender_translated VARCHAR(30),
    FOREIGN KEY (genderId) REFERENCES gender (id)
);

CREATE TABLE IF NOT EXISTS users (
    id BIGINT AUTO_INCREMENT,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    created_at BIGINT,
    gender INT,
    age INT,
    email VARCHAR(100),
    latitude DOUBLE(13, 10),
    longitude DOUBLE(13, 10),
    description TEXT(10000),
    encrypted_password CHAR(60) CHARACTER SET latin1 COLLATE latin1_bin,
    enabled BOOLEAN,
    PRIMARY KEY(id),
    FOREIGN KEY (gender) REFERENCES gender(id)
);

CREATE TABLE IF NOT EXISTS token (
    id BIGINT AUTO_INCREMENT,
    created_at BIGINT,
    user_id BIGINT,
    token CHAR(60),
    PRIMARY KEY(id),
    FOREIGN KEY (user_id) REFERENCES users (id)
)
COLLATE latin1_bin;

CREATE TABLE IF NOT EXISTS interests (
    id int PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS `interests_learner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `interest_id` int(11) DEFAULT NULL,
  `min_level` int(11) DEFAULT NULL,
  `min_language_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `interest_id` (`interest_id`),
  CONSTRAINT `interests_learner_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `interests_learner_ibfk_2` FOREIGN KEY (`interest_id`) REFERENCES `interests` (`id`),
  CONSTRAINT ui UNIQUE (user_id, interest_id)
);

CREATE TABLE IF NOT EXISTS `interests_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `interest_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `interest_id` (`interest_id`),
  CONSTRAINT `interests_teacher_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `interests_teacher_ibfk_2` FOREIGN KEY (`interest_id`) REFERENCES `interests` (`id`)
);

CREATE TABLE IF NOT EXISTS `interests_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interest_id` int(11) DEFAULT NULL,
  `iso_code` char(2) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_code` (`iso_code`),
  KEY `interest_id` (`interest_id`),
  CONSTRAINT `interests_translation_ibfk_1` FOREIGN KEY (`iso_code`) REFERENCES `languages` (`iso_code`),
  CONSTRAINT `interests_translation_ibfk_2` FOREIGN KEY (`interest_id`) REFERENCES `interests` (`id`)
);

CREATE TABLE IF NOT EXISTS `stars` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `given_by` bigint(20) DEFAULT NULL,
  `given_to` bigint(20) DEFAULT NULL,
  `number_of_stars` int(11) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `given_by` (`given_by`),
  KEY `given_to` (`given_to`),
  CONSTRAINT `stars_ibfk_1` FOREIGN KEY (`given_by`) REFERENCES `users` (`id`),
  CONSTRAINT `stars_ibfk_2` FOREIGN KEY (`given_to`) REFERENCES `users` (`id`)
);

CREATE TABLE IF NOT EXISTS `users_languages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20),
  `iso_code` char(2) DEFAULT NULL,
  `proficiency` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`, `user_id`),
  CONSTRAINT `users_languages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `users_languages_ibfk_2` FOREIGN KEY (`iso_code`) REFERENCES `languages` (`iso_code`)
);

CREATE OR REPLACE VIEW star_user AS (
SELECT given_to, AVG(number_of_stars) as average, COUNT(number_of_stars) as total
FROM stars
GROUP BY given_to
);

CREATE OR REPLACE VIEW user_view AS (
    SELECT id as id_user, first_name, last_name, gender, age, email, description
    FROM users
);

CREATE TABLE IF NOT EXISTS users_pictures (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT,
    picture_name VARCHAR(10),
    FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `message` varchar(2000) COLLATE utf8_bin NOT NULL,
  `sendAt` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `preferences` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`user_id` bigint(20),
	`min_age` int,
	`max_age` int,
	`max_distance` int,
	CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);



--SELECT *
--FROM user_view, (
--SELECT interests_teacher.user_id, interests_teacher.interest_id, interests_teacher.level, distance
--FROM interests_learner, interests_teacher, (
--SELECT id, distance
--  FROM (
-- SELECT
--        z.id,
--        z.latitude, z.longitude,
--        p.radius,
--        p.distance_unit
--                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))
--                 * COS(RADIANS(z.latitude))
--                 * COS(RADIANS(p.longpoint - z.longitude))
--                 + SIN(RADIANS(p.latpoint))
--                 * SIN(RADIANS(z.latitude)))) AS distance
--  FROM users AS z
--  JOIN (   /* these are the query parameters */
--        SELECT  latitude  AS latpoint,  longitude AS longpoint,
--                200.0 AS radius,      111.045 AS distance_unit
--                FROM users where id = 1
--    ) AS p ON 1=1
--  WHERE z.latitude
--     BETWEEN p.latpoint  - (p.radius / p.distance_unit)
--         AND p.latpoint  + (p.radius / p.distance_unit)
--    AND z.longitude
--     BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
--         AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
-- ) AS d
-- WHERE distance <= radius AND d.id <> 1
--) as dq
--WHERE interests_learner.user_id = 1
--	AND interests_learner.interest_id = interests_teacher.interest_id
--	AND interests_learner.min_level <= interests_teacher.level
--        AND dq.id = interests_teacher.user_id
--        AND interests_teacher.user_id IN (
--         SELECT id
--  FROM (
-- SELECT
--        z.id,
--        z.latitude, z.longitude,
--        p.radius,
--        p.distance_unit
--                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))
--                 * COS(RADIANS(z.latitude))
--                 * COS(RADIANS(p.longpoint - z.longitude))
--                 + SIN(RADIANS(p.latpoint))
--                 * SIN(RADIANS(z.latitude)))) AS distance
--  FROM users AS z
--  JOIN (   /* these are the query parameters */
--        SELECT  latitude  AS latpoint,  longitude AS longpoint,
--                200.0 AS radius,      111.045 AS distance_unit
--                FROM users where id = 1
--    ) AS p ON 1=1
--  WHERE z.latitude
--     BETWEEN p.latpoint  - (p.radius / p.distance_unit)
--         AND p.latpoint  + (p.radius / p.distance_unit)
--    AND z.longitude
--     BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
--         AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
-- ) AS d
-- WHERE distance <= radius AND d.id <> 1
--        )
--	AND EXISTS (SELECT *
--		    FROM users_languages
--                    WHERE user_id = 1
--		 	AND iso_code in(SELECT iso_code
--                                        FROM users_languages
--                                        WHERE user_id = interests_teacher.user_id
--                                        AND proficiency >= min_language_level
--                                        )
--                    )
--       AND EXISTS(SELECT *
--                  FROM interests_learner as l2, interests_teacher as t2
--                  WHERE l2.user_id = interests_teacher.user_id
--		          AND l2.interest_id = t2.interest_id
--		          AND l2.min_level <= t2.level
--			  AND EXISTS (SELECT *
--				      FROM users_languages
--				      WHERE user_id = interests_learner.user_id
--				 	  AND iso_code in(SELECT iso_code
--				                          FROM users_languages
--				                          WHERE user_id = interests_learner.user_id
--				                          AND proficiency >= min_language_level
--				                          )
--				      )
--                  )
--) as interest_query, star_user
--where user_id = id_user and given_to = user_id
--order by average DESC;






