window.onload =  function getLocation() {

  if (navigator.geolocation &&
        window.localStorage.getItem("geolocation") != "enabled") {
    window.localStorage.setItem("geolocation", "enabled");
    navigator.geolocation.getCurrentPosition(showPosition, showError);
  }
};

function showPosition(position) {
  document.getElementById("latitude").value = position.coords.latitude;
  document.getElementById("longitude").value = position.coords.longitude;
}

function showError(error, error2) {
  window.localStorage.setItem("geolocation", "error");
  switch(error.code) {
    case error.PERMISSION_DENIED:
      alert(map["javascript.userDeniedAccess"]);
      break;
    case error.POSITION_UNAVAILABLE:
      console.log("Location information is unavailable.");
      break;
    case error.TIMEOUT:
      console.log("The request to get user location timed out.");
      break;
    case error.UNKNOWN_ERROR:
      console.log("An unknown error occurred.");
      break;
  }
}
